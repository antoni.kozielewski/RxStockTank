package org.hou.stocktank.rx.utils;

import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.io.*;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Function;

public class NonLockingFileReaderPublisher implements Publisher<String> {
    public static final int DEFAULT_QUEUE_MAX_SIZE = 10000;
    public static final int DEFAULT_CHECK_PERIOD = 500;
    private final String filePath;
    private final int queueMaxSize;
    private Function<String, Boolean> startLinePredicate;
    private EndOfLineChars endOfLine;
    private long currentPosition = 0;
    private List<Subscriber<? super String>> subscribers = new CopyOnWriteArrayList<>();//new ArrayList<>();
    private final SpscLinkedArrayQueue<String> queue;
    private Thread monitorThread;
    private Thread notifierThread;


    public enum EndOfLineChars {
        WINDOWS_EOL(2),
        LINUX_EOL(1);
        private int length;

        EndOfLineChars(int length) {
            this.length = length;
        }

        public int getLength() {
            return length;
        }
    }

    public NonLockingFileReaderPublisher(String filePath, EndOfLineChars endOfLine) {
        this(filePath, DEFAULT_CHECK_PERIOD, DEFAULT_QUEUE_MAX_SIZE, endOfLine, null);
    }

    /**
     * @param filePath    - path to file
     * @param checkPeriod - how long sleep between check for new data
     * @param endOfLine   - end of lines characters
     */
    public NonLockingFileReaderPublisher(String filePath, int checkPeriod, EndOfLineChars endOfLine) {
        this(filePath, checkPeriod, DEFAULT_QUEUE_MAX_SIZE, endOfLine, null);
    }

    /**
     * @param filePath     - path to file
     * @param checkPeriod  - how long sleep between check for new data
     * @param queueMaxSize - how big queue between reading thead and receiver
     * @param endOfLine    - end of lines characters
     */
    public NonLockingFileReaderPublisher(String filePath, int checkPeriod, int queueMaxSize, EndOfLineChars endOfLine) {
        this(filePath, checkPeriod, queueMaxSize, endOfLine, null);
    }

    /**
     * @param filePath           - path to file
     * @param checkPeriod        - how long sleep between check for new data
     * @param queueMaxSize       - how big queue between reading thead and receiver
     * @param endOfLine          - end of lines characters
     * @param startLinePredicate - function that should return true on start line
     */

    public NonLockingFileReaderPublisher(String filePath, int checkPeriod, int queueMaxSize, EndOfLineChars endOfLine, Function<String, Boolean> startLinePredicate) {
        this.filePath = filePath;
        this.endOfLine = endOfLine;
        this.queueMaxSize = queueMaxSize;
        this.startLinePredicate = startLinePredicate;
        queue = new SpscLinkedArrayQueue<>(queueMaxSize);
        notifierThread = new Thread(() -> {
            while (true) {
                // send what is to send
                while (!queue.isEmpty()) {
                    if (!subscribers.isEmpty()) {
                        String newLine = queue.poll();
                        for (Subscriber<? super String> subscriber : subscribers) {
                            subscriber.onNext(newLine);
                        }
                    } else {
                        try {
                            //sleep
                            Thread.sleep(checkPeriod);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                // sleep
                try {
                    Thread.sleep(checkPeriod);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        notifierThread.setName("notifierThread");
        notifierThread.start();

        // find start position if predicate is given
        if (startLinePredicate != null) {
            if (!findStartPosition()) {
                throw new RuntimeException("Declared predicate didn't find start line. Please check predicate and the data file.");
            }
        }


        monitorThread = new Thread(() -> {
            while (true) {
                File file = new File(filePath);
                if (!file.exists()) {
                    throw new RuntimeException("Data file does not exist [" + filePath + "]");
                }
                if ((file.length() > currentPosition) && (queue.size() < queueMaxSize)) {
                    try {
                        InputStream inputStream = new FileInputStream(file);
                        inputStream.skip(currentPosition); // skip all already read data
                        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                        int counter = 0;
                        String line;
                        while (((line = reader.readLine()) != null) && (queue.size() < queueMaxSize)) {
                            queue.offer(line);
                            counter = counter + line.getBytes().length + endOfLine.getLength(); // FIXME refactor it to read bytes not lines and +2 is only CRLF as a end of line - piece of shit code
                        }
                        currentPosition = currentPosition + counter;
                        reader.close();
                        inputStream.close();
                    } catch (FileNotFoundException fnf) {
                        throw new RuntimeException(fnf);
                    } catch (Throwable e) {
                        // do nothing becasue it's expected that there will be conflict in reading file - locks ...
                    }
                } else {
                    try {
                        Thread.sleep(checkPeriod);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        monitorThread.setName("file monitor");
        monitorThread.start();
    }

    private boolean findStartPosition() {
        File file = new File(filePath);
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            int counter = 0;
            String line;
            while (((line = reader.readLine()) != null) && (queue.size() < queueMaxSize)) {
                if (startLinePredicate.apply(line)) {
                    currentPosition = counter;
                    return true;
                }
                counter = counter + line.getBytes().length + endOfLine.getLength(); // FIXME refactor it to read bytes not lines and +2 is only CRLF as a end of line - piece of shit code
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void subscribe(Subscriber<? super String> subscriber) {
        subscriber.onSubscribe(new Subscription() {
            @Override
            public void request(long l) {

            }

            @Override
            public void cancel() {

            }
        });
        subscribers.add(subscriber);
    }
}
