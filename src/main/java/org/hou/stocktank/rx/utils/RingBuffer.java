package org.hou.stocktank.rx.utils;


import java.util.Iterator;
import java.util.LinkedList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RingBuffer<T> implements Iterable<T> {
    private LinkedList<T> buffer = new LinkedList<T>();
    private int maxSize = 1;

    public RingBuffer(int size) {
        this.maxSize = size;
    }

    public void put(T value) {
        if (buffer.size() >= maxSize) {
            buffer.removeFirst();
        }
        buffer.add(value);
    }

    public T get(int index) {
        return buffer.get(index);
    }

    public int getSize() {
        return buffer.size();
    }

    public int getMaxSize() {
        return maxSize;
    }

    public boolean isEmpty() {
        return buffer.size() - maxSize == 0;
    }

    public boolean isFull() {
        return buffer.size() == maxSize;
    }

    public Stream<T> stream() {
        return buffer.stream();
    }

    @Override
    public Iterator<T> iterator() {
        return buffer.iterator();
    }

    @Override
    public String toString() {
        return this.buffer.stream().map(s -> s.toString()).collect(Collectors.joining(",", "[", "]"));
    }
}
