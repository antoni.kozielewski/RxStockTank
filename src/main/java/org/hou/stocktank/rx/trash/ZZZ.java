package org.hou.stocktank.rx.trash;


import io.reactivex.Flowable;
import org.hou.stocktank.rx.base.*;
import org.hou.stocktank.rx.indicators.RxSMAIndicator;
import org.hou.stocktank.rx.providers.StockStreamAggregator;
import org.hou.stocktank.rx.providers.bossa.BossaIntradayStreamsProvider;

public class ZZZ {
    private static final String path = "C:\\Users\\Ant\\Documents\\mStatica 4\\Pliki CSV\\";

    public static void main(String[] args) {
        String stockName = "CDR";
        int lastNDays = 3000;
        BossaIntradayStreamsProvider streamsProvider = new BossaIntradayStreamsProvider(path, lastNDays);
        StockStream<Transaction> source = streamsProvider.getStockStream(stockName);
        Flowable<Tick> dayAggregatedStream = StockStreamAggregator.getDayAggregatedStream(source);

        Flowable<Decimal> closePriceStream = dayAggregatedStream.map(Tick::getClosePrice).publish().refCount();
        Flowable<Decimal> sma200 = new RxSMAIndicator(200).calculate(closePriceStream);
        Flowable<Decimal> sma100 = new RxSMAIndicator(100).calculate(closePriceStream);
        Flowable<Decimal> sma50 = new RxSMAIndicator(50).calculate(closePriceStream);
        Flowable<Decimal> sma20 = new RxSMAIndicator(20).calculate(closePriceStream);

        Flowable.zip(dayAggregatedStream, sma200, sma100, sma50, sma20,
                (tick, s200, s100, s50, s20) -> new DecimalResults(tick).addValue("sma200", s200).addValue("sma100", s100).addValue("sma50", s50).addValue("sma20", s20))
                .subscribe(data -> System.out.println(data.getTick().getStartTimestamp() + " | " + data.getValue("sma200") + " | " + data.getValue("sma100") + " | " + data.getValue("sma50") + " | " + data.getValue("sma20")));
    }
}
