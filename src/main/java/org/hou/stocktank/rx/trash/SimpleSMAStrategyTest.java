package org.hou.stocktank.rx.trash;


import io.reactivex.Flowable;
import org.hou.stocktank.rx.base.Decimal;
import org.hou.stocktank.rx.base.Tick;
import org.hou.stocktank.rx.indicators.RxSMAIndicator;
import org.hou.stocktank.rx.providers.StockStreamAggregator;
import org.hou.stocktank.rx.providers.bossa.BossaIntradayStreamsProvider;

public class SimpleSMAStrategyTest {
    private static final String path = "C:\\Users\\Ant\\Documents\\mStatica 4\\Pliki CSV\\";

    public class StrategyResult {
        protected Tick tick;
        protected String signal;
        protected Decimal sma;

        public StrategyResult(Tick tick, Decimal sma, String signal) {
            this.tick = tick;
            this.sma = sma;
            this.signal = signal;
        }

        public Decimal getSma() {
            return sma;
        }

        public Tick getTick() {
            return tick;
        }

        public String getSignal() {
            return signal;
        }
    }

    public void run() {
        String ticker = "CDR";
        int lastNDays = 30;
        BossaIntradayStreamsProvider streamsProvider = new BossaIntradayStreamsProvider(path, lastNDays);
        Flowable<Tick> sourceStreamMin = StockStreamAggregator.getMinuteAggregatedStream(streamsProvider.getStockStream(ticker));
        Flowable<Tick> sourceStreamHour = StockStreamAggregator.getHourAggregatedStream(streamsProvider.getStockStream(ticker));
        //Flowable<Tick> sourceStream = streamsProvider.getDayAggregatedStream().publish().refCount();


//        sourceStream.subscribe(System.out::println);

        RxSMAIndicator sma200Indicator = new RxSMAIndicator(200);


        Flowable<String> stringFlowable = Flowable.combineLatest(sourceStreamMin, sma200Indicator.calculate(sourceStreamHour.map(Tick::getClosePrice)), (tm, th) -> tm.getStartTimestamp() + " | " + th);

        stringFlowable.subscribe(System.out::println);

//        RxSMAIndicator sma200Indicator = new RxSMAIndicator(200);
//        RxSMAIndicator sma100Indicator = new RxSMAIndicator(100);
//        RxSMAIndicator sma50Indicator = new RxSMAIndicator(50);
//        RxSMAIndicator sma20Indicator = new RxSMAIndicator(20);
//
//        Flowable<Decimal> sma200 = sma200Indicator.calculate(sourceStream.map(Tick::getClosePrice));
//        Flowable<Decimal> sma100 = sma100Indicator.calculate(sourceStream.map(Tick::getClosePrice));
//        Flowable<Decimal> sma50 = sma50Indicator.calculate(sourceStream.map(Tick::getClosePrice));
//        Flowable<Decimal> sma20 = sma20Indicator.calculate(sourceStream.map(Tick::getClosePrice));

//        String smaMask = "sma20";
//        sourceStream
//                .map(tick -> new DecimalResults(tick))
//                .zipWith(sma20, (result, sma) -> result.addValue(smaMask, sma))
//                .flatMap(new Function<DecimalResults, Publisher<String>>() {
//                    private DecimalResults previous;
//
//                    @Override
//                    public Publisher<String> apply(@NonNull DecimalResults current) throws Exception {
//                        if (previous == null){
//                            previous = current;
//                            return Flowable.empty();
//                        } else {
//                            if (current.crossesUp(DecimalResults.TICK_CLOSE_PRICE, smaMask, previous)) {
//                                previous = current;
//                                return Flowable.just("----------------up   | " + current.getA().getStartTimestamp());
//                            }
//                            if (current.crossesDown(DecimalResults.TICK_CLOSE_PRICE, smaMask, previous)) {
//                                previous = current;
//                                return Flowable.just("----------------down | " + current.getA().getStartTimestamp());
//                            }
//                            previous = current;
//                            return Flowable.empty();
//                        }
//                    }
//                })
//                .subscribe(System.out::println);


//        sourceStream
//                .map(tick -> new DecimalResults(tick))
//                .zipWith(sma200, (result, sma) -> result.addValue("sma200", sma))
////                .zipWith(sma100, (result, sma) -> result.addValue("sma100", sma))
////                .zipWith(sma50, (result, sma) -> result.addValue("sma50", sma))
////                .zipWith(sma20, (result, sma) -> result.addValue("sma20", sma))
//                .flatMap(new Function<DecimalResults, Publisher<String>>() {
//                    DecimalResults previous;
//
//                    @Override
//                    public Publisher<String> apply(DecimalResults current) throws Exception {
//                        if (previous == null) {
//                            this.previous = current;
//                            return Flowable.empty();
//                        }
//                        if(current.crossesDown(DecimalResults.TICK_CLOSE_PRICE, "sma200", previous)) {
//                            System.out.println(" close price crosses down sma200 ");
//                        }
//
//                        return null;
//                    }
//                })
//
//                .blockingNext()
//                .forEach(System.out::println);
//
    }

    public static void main(String[] args) {
        SimpleSMAStrategyTest test = new SimpleSMAStrategyTest();
        test.run();
    }
}
