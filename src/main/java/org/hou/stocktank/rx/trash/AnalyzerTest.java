package org.hou.stocktank.rx.trash;

import io.reactivex.Flowable;
import org.hou.stocktank.rx.analyzers.SMAAnalyzer;
import org.hou.stocktank.rx.base.Decimal;
import org.hou.stocktank.rx.base.StockStream;
import org.hou.stocktank.rx.base.Tick;
import org.hou.stocktank.rx.base.Transaction;
import org.hou.stocktank.rx.indicators.RxSMAIndicator;
import org.hou.stocktank.rx.providers.StockStreamAggregator;
import org.hou.stocktank.rx.providers.bossa.BossaIntradayStreamsProvider;

public class AnalyzerTest {

    public static void main(String[] args) {
        String ticker = "CDR";
        int lastNDays = 3000;
        final String path = "C:\\Users\\Ant\\Documents\\mStatica 4\\Pliki CSV\\";
        BossaIntradayStreamsProvider streamsProvider = new BossaIntradayStreamsProvider(path, lastNDays);
        StockStream<Transaction> source = streamsProvider.getStockStream(ticker);
        Flowable<Tick> tickSource = StockStreamAggregator.getMinuteAggregatedStream(source);
        Flowable<Decimal> sourceStream = StockStreamAggregator.getDayAggregatedStream(source).map(Tick::getClosePrice).publish().refCount();
        Flowable<Decimal> sma200 = new RxSMAIndicator(200).calculate(sourceStream);
        Flowable<Decimal> sma100 = new RxSMAIndicator(100).calculate(sourceStream);
        Flowable<Decimal> sma50 = new RxSMAIndicator(50).calculate(sourceStream);
        Flowable<Decimal> sma20 = new RxSMAIndicator(20).calculate(sourceStream);
        SMAAnalyzer smaMinutesAnalyzer = new SMAAnalyzer(tickSource, sma200, sma100, sma50, sma20);
        smaMinutesAnalyzer.analyze()
                .filter(x -> x.getStrength() == 200)
                .subscribe(System.out::println);
    }
}
