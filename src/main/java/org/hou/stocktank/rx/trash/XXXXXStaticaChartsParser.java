package org.hou.stocktank.rx.trash;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.FileInputStream;
import java.io.IOException;


public class XXXXXStaticaChartsParser {
    private static String XPATH_FORMATIONS = "/ChartObject/Variable[@Name='ChartLogic']/Variable[@Name='Formations']/Variable[@Version='1']";
    private static String XPATH_FORMATION_NAME = "Variable[@Name='Label']/Variable[@Name='Name']";
    private static String XPATH_FORMATION_CONTROLPOINTS_POINT_A_X = "Variable[@Name='ControlPoints']/Variable[1]/Variable[@Name='CorX']";
    private static String XPATH_FORMATION_CONTROLPOINTS_POINT_A_Y = "Variable[@Name='ControlPoints']/Variable[1]/Variable[@Name='CorY']";
    private static String XPATH_FORMATION_CONTROLPOINTS_POINT_B_X = "Variable[@Name='ControlPoints']/Variable[2]/Variable[@Name='CorX']";
    private static String XPATH_FORMATION_CONTROLPOINTS_POINT_B_Y = "Variable[@Name='ControlPoints']/Variable[2]/Variable[@Name='CorY']";
    private static String XPATH_FORMATION_FORMATION_TYPE = "Variable[@Name='Type']";


    public void run() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        String filePath = "d:/a004/Monitor/Charts/WIG20 (Dzienny).n4sChart";
        FileInputStream fileIS = new FileInputStream(filePath);
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        Document xmlDocument = builder.parse(fileIS);
        XPath xPath = XPathFactory.newInstance().newXPath();
        NodeList nodeList = (NodeList) xPath.compile(XPATH_FORMATIONS).evaluate(xmlDocument, XPathConstants.NODESET);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node formation = nodeList.item(i);
            Node nameNode = (Node) xPath.compile(XPATH_FORMATION_NAME).evaluate(formation, XPathConstants.NODE);
            String name = nameNode.getAttributes().getNamedItem("Value").toString();

            Node typeNode = (Node) xPath.compile(XPATH_FORMATION_FORMATION_TYPE).evaluate(formation, XPathConstants.NODE);
            String type = typeNode.getAttributes().getNamedItem("Value").toString();

            Node pointAxNode = (Node) xPath.compile(XPATH_FORMATION_CONTROLPOINTS_POINT_A_X).evaluate(formation, XPathConstants.NODE);
            String pointAx = pointAxNode.getAttributes().getNamedItem("Value").toString();

            Node pointAyNode = (Node) xPath.compile(XPATH_FORMATION_CONTROLPOINTS_POINT_A_Y).evaluate(formation, XPathConstants.NODE);
            String pointAy = pointAyNode.getAttributes().getNamedItem("Value").toString();

            String pointBx = "";
            String pointBy = "";
            if (type.equals("Value=\"4\"")) {
                Node pointBxNode = (Node) xPath.compile(XPATH_FORMATION_CONTROLPOINTS_POINT_B_X).evaluate(formation, XPathConstants.NODE);
                pointBx = pointBxNode.getAttributes().getNamedItem("Value").toString();

                Node pointByNode = (Node) xPath.compile(XPATH_FORMATION_CONTROLPOINTS_POINT_B_Y).evaluate(formation, XPathConstants.NODE);
                pointBy = pointByNode.getAttributes().getNamedItem("Value").toString();
            }


            System.out.println("type = " + type);
            System.out.println("Name : " + name);
            System.out.println(" point A X : " + pointAx);
            System.out.println(" point A Y : " + pointAy);
            System.out.println(" point B X : " + pointBx);
            System.out.println(" point B Y : " + pointBy);
            System.out.println("-----------------------------------------------------------------------------------------------------------");
        }
    }


    public static void main(String[] args) throws Exception {
        XXXXXStaticaChartsParser parser = new XXXXXStaticaChartsParser();
        parser.run();
    }
}
