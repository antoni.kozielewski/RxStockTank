package org.hou.stocktank.rx.trash;


import io.reactivex.Flowable;
import org.hou.stocktank.rx.base.*;
import org.hou.stocktank.rx.indicators.RxSARIndicator;
import org.hou.stocktank.rx.providers.StockStreamAggregator;
import org.hou.stocktank.rx.providers.bossa.BossaIntradayStreamsProvider;
import org.hou.stocktank.rx.strategies.PriceCrossesSignalStrategy;
import org.hou.stocktank.rx.strategies.RxStrategy;
import org.hou.stocktank.rx.strategies.TimeTresholdStrategyFilter;
import org.hou.stocktank.rx.strategies.evaluation.StrategyEvaluator;
import org.joda.time.DateTime;

public class SARStrategyEvaluationTest {
    private static final String path = "C:\\Users\\Ant\\Documents\\mStatica 4\\Pliki CSV\\";

    public void run() {
        System.out.println(" start evaluation ");

        String stockName = "CDR";
        int lastNDays = 3000;

        BossaIntradayStreamsProvider streamsProvider = new BossaIntradayStreamsProvider(path, lastNDays);
        StockStream<Transaction> source = streamsProvider.getStockStream(stockName);
        Flowable<Tick> minuteAggregatedStream = StockStreamAggregator.getMinuteAggregatedStream(source);
        Flowable<Decimal> sma = new RxSARIndicator(0.02, 0.02, 0.2).calculate(minuteAggregatedStream);

        RxStrategy baseStrategy = new PriceCrossesSignalStrategy(minuteAggregatedStream, sma);
        RxStrategy strategy = new TimeTresholdStrategyFilter(baseStrategy, 60 * 9);

        StrategyEvaluator evaluator = new StrategyEvaluator(strategy)
                .withStartAccountState(Decimal.valueOf(10000))
                //.withStartDateTime(DateTime.now().minusDays(300))
                ;
        evaluator.evaluate()
                .filter(result -> result.getSignal().getOperation() == Operation.BUY || result.getSignal().getOperation() == Operation.SELL || result.getDateTime().isAfter(DateTime.now().minusDays(1)))
                .subscribe(System.out::println);

    }

    public static void main(String[] args) {
        SARStrategyEvaluationTest test = new SARStrategyEvaluationTest();
        test.run();
    }
}
