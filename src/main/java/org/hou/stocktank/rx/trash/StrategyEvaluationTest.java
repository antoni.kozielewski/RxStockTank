package org.hou.stocktank.rx.trash;


import io.reactivex.Flowable;
import org.hou.stocktank.rx.base.Decimal;
import org.hou.stocktank.rx.base.Operation;
import org.hou.stocktank.rx.base.Tick;
import org.hou.stocktank.rx.indicators.RxSMAIndicator;
import org.hou.stocktank.rx.providers.StockStreamAggregator;
import org.hou.stocktank.rx.providers.bossa.BossaIntradayStreamsProvider;
import org.hou.stocktank.rx.strategies.ClosePriceCrossesSignalStrategy;
import org.hou.stocktank.rx.strategies.RxStrategy;
import org.hou.stocktank.rx.strategies.TimeTresholdStrategyFilter;
import org.hou.stocktank.rx.strategies.evaluation.StrategyEvaluator;
import org.joda.time.DateTime;

public class StrategyEvaluationTest {
    private static final String path = "C:\\Users\\Ant\\Documents\\mStatica 4\\Pliki CSV\\";

    public void run() {
        System.out.println(" start evaluation ");

        String stockName = "CDR";
        int lastNDays = 3000;
        BossaIntradayStreamsProvider streamProvider = new BossaIntradayStreamsProvider(path, lastNDays);
        Flowable<Tick> source = StockStreamAggregator.getMinuteAggregatedStream(streamProvider.getStockStream(stockName));

        Flowable<Decimal> sma = new RxSMAIndicator(200).calculate(source.map(Tick::getClosePrice));

        RxStrategy baseStrategy = new ClosePriceCrossesSignalStrategy(source, sma);
        RxStrategy strategy = new TimeTresholdStrategyFilter(baseStrategy, 60 * 9);

        StrategyEvaluator evaluator = new StrategyEvaluator(strategy)
                .withStartAccountState(Decimal.valueOf(10000))
                //.withStartDateTime(DateTime.now().minusDays(300))
                ;
        evaluator.evaluate()
                .filter(result -> result.getSignal().getOperation() == Operation.BUY || result.getSignal().getOperation() == Operation.SELL || result.getDateTime().isAfter(DateTime.now().minusDays(1)))
                .subscribe(System.out::println);

    }

    public static void main(String[] args) {
        StrategyEvaluationTest test = new StrategyEvaluationTest();
        test.run();
    }
}
