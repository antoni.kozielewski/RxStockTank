package org.hou.stocktank.rx.trash;

import io.reactivex.Flowable;
import org.hou.stocktank.rx.base.Decimal;
import org.hou.stocktank.rx.base.Tick;
import org.hou.stocktank.rx.indicators.RxRSIIndicator;
import org.hou.stocktank.rx.indicators.RxSMAIndicator;
import org.hou.stocktank.rx.indicators.RxStochasticOscillatorDIndicator;
import org.hou.stocktank.rx.indicators.RxStochasticOscillatorKIndicator;
import org.hou.stocktank.rx.providers.StockStreamAggregator;
import org.hou.stocktank.rx.providers.bossa.BossaIntradayStreamsProvider;


public class YYY {
    private static final String path = "C:\\Users\\Kozielew\\Documents\\mStatica 4\\Pliki CSV\\";

    public static void main(String[] args) throws InterruptedException {
        String ticker = "CDR";
        BossaIntradayStreamsProvider streamsProvider = new BossaIntradayStreamsProvider(path, 3000);

        System.out.println(" started for stockName: " + ticker);
        //Flowable<Tick> stream = streamsProvider.getHourAggregatedStream().publish().refCount();
        Flowable<Tick> stream = StockStreamAggregator.getMinuteAggregatedStream(streamsProvider.getStockStream(ticker));

        RxSMAIndicator smaIndicator = new RxSMAIndicator(20);
        RxRSIIndicator rsiIndicator = new RxRSIIndicator(20);
        RxStochasticOscillatorKIndicator kIndicator = new RxStochasticOscillatorKIndicator(20);
        RxStochasticOscillatorDIndicator dIndicator = new RxStochasticOscillatorDIndicator(kIndicator, 4);

        Flowable<Decimal> streamSMA = smaIndicator.calculate(stream.map(tick -> tick.getClosePrice()));
        Flowable<Decimal> streamRSI = rsiIndicator.calculate(stream.map(tick -> tick.getClosePrice()));
        Flowable<Decimal> streamStochasticK = kIndicator.calculate(stream);
        Flowable<Decimal> streamStochasticD = dIndicator.calculate(stream);

        stream.zipWith(streamSMA, (src, sma) -> src.getStartTimestamp() + " | c " + src.getClosePrice() + " | " + sma)
                .zipWith(streamRSI, (src, rsi) -> src + " |  " + rsi)
                .zipWith(streamStochasticK, (src, k) -> src + " | " + k)
                .zipWith(streamStochasticD, (src, d) -> src + " | " + d)
                .subscribe(System.out::println);

        System.out.println(" done ");

    }
}
