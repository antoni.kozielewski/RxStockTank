package org.hou.stocktank.rx.trash;


import io.reactivex.Flowable;
import org.hou.stocktank.rx.base.Decimal;
import org.hou.stocktank.rx.base.Operation;
import org.hou.stocktank.rx.base.Tick;
import org.hou.stocktank.rx.indicators.RxSARIndicator;
import org.hou.stocktank.rx.indicators.RxSMAIndicator;
import org.hou.stocktank.rx.providers.StockStreamAggregator;
import org.hou.stocktank.rx.providers.bossa.BossaIntradayStreamsProvider;
import org.hou.stocktank.rx.strategies.ACrossesBStrategy;
import org.hou.stocktank.rx.strategies.evaluation.StrategyEvaluator;
import org.joda.time.DateTime;

public class StrategyEvaluationTest2 {
    private static final String path = "C:\\Users\\kozielew\\Documents\\mStatica 4\\Pliki CSV\\";

    public void run() {
        System.out.println(" start evaluation ");

        String stockName = "CDR";
        int lastNDays = 3000;

        BossaIntradayStreamsProvider streamsProvider = new BossaIntradayStreamsProvider(path, lastNDays);
        Flowable<Tick> minuteAggregatedStream = StockStreamAggregator.getMinuteAggregatedStream(streamsProvider.getStockStream(stockName));

        Flowable<Decimal> sma = new RxSMAIndicator(10).calculate(minuteAggregatedStream.map(Tick::getClosePrice));

        RxSARIndicator sarIndicator = new RxSARIndicator(0.02, 0.02, 0.2);

        ACrossesBStrategy strategy = new ACrossesBStrategy(minuteAggregatedStream, minuteAggregatedStream.map(Tick::getClosePrice), sarIndicator.calculate(minuteAggregatedStream));


        StrategyEvaluator evaluator = new StrategyEvaluator(strategy)
                .withStartAccountState(Decimal.valueOf(10000));
        evaluator.evaluate()
                .filter(result -> result.getSignal().getOperation() == Operation.BUY || result.getSignal().getOperation() == Operation.SELL || result.getDateTime().isAfter(DateTime.now().minusDays(2)))
                .subscribe(System.out::println);

    }

    public static void main(String[] args) {
        StrategyEvaluationTest2 test = new StrategyEvaluationTest2();
        test.run();
    }
}
