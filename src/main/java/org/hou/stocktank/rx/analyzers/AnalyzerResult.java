package org.hou.stocktank.rx.analyzers;

import org.hou.stocktank.rx.base.Tick;
import org.hou.stocktank.rx.base.Trend;

public class AnalyzerResult {
    protected Tick tick;
    protected Trend trend;
    protected int strength;
    protected String description;

    public AnalyzerResult(Tick tick, Trend trend, int strength, String description) {
        this.tick = tick;
        this.trend = trend;
        this.strength = strength;
        this.description = description;
    }

    public Trend getTrend() {
        return trend;
    }

    public int getStrength() {
        return strength;
    }

    public String getDescription() {
        return description;
    }

    public Tick getTick() {
        return tick;
    }

    public String toString() {
        return tick + " | " + trend + " | strength = " + strength + " | " + description;
    }

}
