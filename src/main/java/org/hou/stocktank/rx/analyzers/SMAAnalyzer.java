package org.hou.stocktank.rx.analyzers;


import io.reactivex.Flowable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import org.hou.stocktank.rx.base.*;
import org.hou.stocktank.rx.indicators.RxSMAIndicator;
import org.reactivestreams.Publisher;

public class SMAAnalyzer implements RxAnalyzer {
    private Flowable<Tick> source;
    private Flowable<Decimal> sma200;
    private Flowable<Decimal> sma100;
    private Flowable<Decimal> sma50;
    private Flowable<Decimal> sma20;


    public SMAAnalyzer(Flowable<Tick> source) {
        this.source = source;
        Flowable<Decimal> tempSource = source.map(tick -> tick.getClosePrice()).publish().refCount();

        this.sma200 = new RxSMAIndicator(200).calculate(tempSource);
        this.sma100 = new RxSMAIndicator(100).calculate(tempSource);
        this.sma50 = new RxSMAIndicator(50).calculate(tempSource);
        this.sma20 = new RxSMAIndicator(20).calculate(tempSource);
    }

    public SMAAnalyzer(Flowable<Tick> source, Flowable<Decimal> sma200, Flowable<Decimal> sma100, Flowable<Decimal> sma50, Flowable<Decimal> sma20) {
        this.source = source;
        this.sma200 = sma200;
        this.sma100 = sma100;
        this.sma50 = sma50;
        this.sma20 = sma20;
    }

    public Flowable<? extends AnalyzerResult> analyze() {
        //return Flowable.zip(source, sma200, sma100, sma50, sma20, (tick, s200, s100, s50, s20) -> new DecimalResults(tick).addValue("sma200", s200).addValue("sma100", s100).addValue("sma50", s50).addValue("sma20", s20))
        return Flowable.combineLatest(source, sma200, sma100, sma50, sma20, (tick, s200, s100, s50, s20) -> new DecimalResults(tick).addValue("sma200", s200).addValue("sma100", s100).addValue("sma50", s50).addValue("sma20", s20))
                .flatMap(new Function<DecimalResults, Publisher<AnalyzerResult>>() {
                             DecimalResults previous = null;

                             @Override
                             public Publisher<AnalyzerResult> apply(@NonNull DecimalResults current) throws Exception {
                                 if (previous == null) {
                                     previous = current;
                                     return Flowable.empty();
                                 }
                                 // CHANGE TO BEARISH
                                 if (current.crossesDown(DecimalResults.TICK_CLOSE_PRICE, "sma200", previous)) {
                                     previous = current;
                                     return Flowable.just(new AnalyzerResult(current.getTick(), Trend.CHANGE_TO_BEARISH, 200, "price crosses down SMA200 [" + current.getValue("sma200") + "]"));
                                 }
                                 if (current.crossesDown(DecimalResults.TICK_CLOSE_PRICE, "sma100", previous)) {
                                     previous = current;
                                     return Flowable.just(new AnalyzerResult(current.getTick(), Trend.CHANGE_TO_BEARISH, 100, "price crosses down SMA100 [" + current.getValue("sma100") + "]"));
                                 }
                                 if (current.crossesDown(DecimalResults.TICK_CLOSE_PRICE, "sma50", previous)) {
                                     previous = current;
                                     return Flowable.just(new AnalyzerResult(current.getTick(), Trend.CHANGE_TO_BEARISH, 50, "price crosses down SMA50 [" + current.getValue("sma50") + "]"));
                                 }
                                 if (current.crossesDown(DecimalResults.TICK_CLOSE_PRICE, "sma20", previous)) {
                                     previous = current;
                                     return Flowable.just(new AnalyzerResult(current.getTick(), Trend.CHANGE_TO_BEARISH, 20, "price crosses down SMA20 [" + current.getValue("sma20") + "]"));
                                 }

                                 // CHANGE TO BULLISH
                                 if (current.crossesUp(DecimalResults.TICK_CLOSE_PRICE, "sma200", previous)) {
                                     previous = current;
                                     return Flowable.just(new AnalyzerResult(current.getTick(), Trend.CHANGE_TO_BULLISH, 200, "price crosses up SMA200 [" + current.getValue("sma200") + "]"));
                                 }
                                 if (current.crossesUp(DecimalResults.TICK_CLOSE_PRICE, "sma100", previous)) {
                                     previous = current;
                                     return Flowable.just(new AnalyzerResult(current.getTick(), Trend.CHANGE_TO_BULLISH, 100, "price crosses up SMA100 [" + current.getValue("sma100") + "]"));
                                 }
                                 if (current.crossesUp(DecimalResults.TICK_CLOSE_PRICE, "sma50", previous)) {
                                     previous = current;
                                     return Flowable.just(new AnalyzerResult(current.getTick(), Trend.CHANGE_TO_BULLISH, 50, "price crosses up SMA50 [" + current.getValue("sma50") + "]"));
                                 }
                                 if (current.crossesUp(DecimalResults.TICK_CLOSE_PRICE, "sma20", previous)) {
                                     previous = current;
                                     return Flowable.just(new AnalyzerResult(current.getTick(), Trend.CHANGE_TO_BULLISH, 20, "price crosses up SMA20 [" + current.getValue("sma20") + "]"));
                                 }

                                 previous = current;
                                 return Flowable.empty();
                             }
                         }
                );
    }

}
