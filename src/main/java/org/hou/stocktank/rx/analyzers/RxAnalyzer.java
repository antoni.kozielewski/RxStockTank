package org.hou.stocktank.rx.analyzers;

import io.reactivex.Flowable;

public interface RxAnalyzer {
    Flowable<? extends AnalyzerResult> analyze();
}
