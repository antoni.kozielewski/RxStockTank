package org.hou.stocktank.rx.indicators;


import io.reactivex.Flowable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import org.hou.stocktank.rx.base.Decimal;
import org.hou.stocktank.rx.base.Tick;

public class RxSARIndicator implements RxIndicator<Tick, Decimal> {
    private Decimal accelerationStart = Decimal.valueOf(0.02); // default 0.02
    private Decimal acceleration = Decimal.valueOf(0.02);      // default 0.02
    private Decimal accelerationMax = Decimal.valueOf(0.20);   // default 0.20


    public enum Direction {
        UP, DOWN;
    }

    public RxSARIndicator() {
    }

    public RxSARIndicator(double accelerationStart, double acceleration, double accelerationMax) {
        this();
        this.accelerationStart = Decimal.valueOf(accelerationStart);
        this.acceleration = Decimal.valueOf(acceleration);
        this.accelerationMax = Decimal.valueOf(accelerationMax);
    }

    private Decimal increaseAcceleration(Decimal currentAF) {
        if (currentAF.isLessThan(accelerationMax)) {
            return currentAF.plus(acceleration);
        }
        return accelerationMax;
    }

    @Override
    public Flowable<Decimal> calculate(Flowable<Tick> inputData) {
        return inputData.map(new Function<Tick, Decimal>() {
            Decimal lowValue = Decimal.valueOf(Double.MAX_VALUE); // min level from last period
            Decimal maxValue = Decimal.ZERO; // max level from last period
            Decimal ep = Decimal.ZERO; // extreme point
            Direction direction = Direction.UP;
            Decimal af = accelerationStart; // acceleration factor
            Decimal lastPSAR = Decimal.ZERO;
            int index = -1;

            @Override
            public Decimal apply(@NonNull Tick tick) throws Exception {
                    index ++;
                    if (index == 0) { // ----- first index always UP
                        direction = Direction.UP;
                        maxValue = tick.getOpenPrice().max(tick.getClosePrice());
                        lowValue = tick.getOpenPrice().min(tick.getClosePrice());
                        ep = maxValue;
                        af = accelerationStart;
                        lastPSAR = lowValue;
                        return lowValue;
                    } else {
                        if (direction == Direction.UP) { // ----- jesli jedziemy w gore to :
                            //Current SAR = Prior SAR + Prior AF(Prior EP - Prior SAR)
                            Decimal sar = lastPSAR.plus(af.multipliedBy(ep.minus(lastPSAR)));
                            lastPSAR = sar;
                            Decimal result = sar;
                            // do we need to increase maxValue ?
                            if (maxValue.isLessThan(tick.getOpenPrice().max(tick.getClosePrice()))) {
                                maxValue = tick.getOpenPrice().max(tick.getClosePrice());
                            }
                            // do wee need to move ep ? because we made need to count every new max and if current tick is new max we increase acceleration
                            if (tick.getClosePrice().max(tick.getOpenPrice()).isGreaterThan(ep)) {
                                ep = tick.getClosePrice().max(tick.getOpenPrice());
                                af = increaseAcceleration(af);
                            } // if sar is coverd by body of tick we have to change direction
                            if (sar.isGreaterThanOrEqual(tick.getOpenPrice().min(tick.getClosePrice()))) {
                                lowValue = Decimal.valueOf(Double.MAX_VALUE);
                                ep = maxValue;
                                af = accelerationStart;
                                lastPSAR = maxValue;
                                direction = Direction.DOWN;
                            }
                            return result;
                        } else {
                            //Current SAR = Prior SAR - Prior AF(Prior SAR - Prior EP)
                            Decimal sar = lastPSAR.minus(af.multipliedBy(lastPSAR.minus(ep)));
                            lastPSAR = sar;
                            Decimal result = sar;
                            // collect maxValue
                            if (lowValue.isGreaterThan(tick.getOpenPrice().max(tick.getClosePrice()))) {
                                lowValue = tick.getOpenPrice().max(tick.getClosePrice());
                            }
                            if (tick.getClosePrice().min(tick.getOpenPrice()).isLessThan(ep)) {
                                ep = tick.getClosePrice().min(tick.getOpenPrice());
                                af = increaseAcceleration(af);
                            }
                            if (sar.isLessThanOrEqual(tick.getClosePrice().max(tick.getOpenPrice()))) {
                                maxValue = Decimal.ZERO;
                                ep = lowValue;
                                lastPSAR = lowValue;
                                af = accelerationStart;
                                direction = Direction.UP;
                            }
                            return result;
                        }
                    }
                }
        });
    }


}
