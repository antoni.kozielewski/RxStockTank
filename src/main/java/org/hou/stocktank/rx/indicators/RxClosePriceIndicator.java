package org.hou.stocktank.rx.indicators;

import io.reactivex.Flowable;
import org.hou.stocktank.rx.base.Decimal;
import org.hou.stocktank.rx.base.Tick;


public class RxClosePriceIndicator implements RxIndicator<Tick, Decimal> {

    @Override
    public Flowable<Decimal> calculate(Flowable<Tick> inputData) {
        return inputData.map(tick -> tick.getClosePrice());
    }

}
