package org.hou.stocktank.rx.indicators;

import io.reactivex.Flowable;
import javafx.util.Pair;
import org.hou.stocktank.rx.base.Decimal;
import org.hou.stocktank.rx.base.Tick;


/**
 *
 */
public class RxStochasticOscillatorKIndicator implements RxIndicator<Tick, Decimal> {
    private final int timeFrame;
    private final RxHighestValueIndicator highestValueIndicator;
    private final RxLowestValueIndicator lowestValueIndicator;
    private final RxClosePriceIndicator closePriceIndicator;
    private final RxMinPriceIndicator minPriceIndicator;
    private final RxMaxPriceIndicator maxPriceIndicator;

    public RxStochasticOscillatorKIndicator(int timeFrame) {
        this.timeFrame = timeFrame;
        highestValueIndicator = new RxHighestValueIndicator(timeFrame);
        lowestValueIndicator = new RxLowestValueIndicator(timeFrame);
        closePriceIndicator = new RxClosePriceIndicator();
        minPriceIndicator = new RxMinPriceIndicator();
        maxPriceIndicator = new RxMaxPriceIndicator();
    }

    @Override
    public Flowable<Decimal> calculate(Flowable<Tick> inputData) {
        RxHighestValueIndicator highestValueIndicator = new RxHighestValueIndicator(timeFrame);
        RxLowestValueIndicator lowestValueIndicator = new RxLowestValueIndicator(timeFrame);
        RxClosePriceIndicator closePriceIndicator = new RxClosePriceIndicator();
        RxMinPriceIndicator minPriceIndicator = new RxMinPriceIndicator();
        RxMaxPriceIndicator maxPriceIndicator = new RxMaxPriceIndicator();

        return
                lowestValueIndicator.calculate(minPriceIndicator.calculate(inputData))
                        .zipWith(
                                highestValueIndicator.calculate(maxPriceIndicator.calculate(inputData)),
                                Pair::new
                        ).
                        zipWith(
                                closePriceIndicator.calculate(inputData),
                                (lowHigh, close) -> (close.minus(lowHigh.getKey())).dividedBy(lowHigh.getValue().minus(lowHigh.getKey())).multipliedBy(Decimal.HUNDRED)
                        );
    }

}
