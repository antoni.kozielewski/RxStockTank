package org.hou.stocktank.rx.indicators;


import io.reactivex.Flowable;
import org.hou.stocktank.rx.base.Decimal;
import org.hou.stocktank.rx.utils.RingBuffer;

public class RxCumulatedLossesIndicator implements RxIndicator<Decimal, Decimal> {
    private int timeFame;

    public RxCumulatedLossesIndicator(int timeFrame) {
        this.timeFame = timeFrame;
    }

    @Override
    public Flowable<Decimal> calculate(Flowable<Decimal> inputData) {
        RingBuffer<Decimal> ringBuffer = new RingBuffer<>(timeFame + 1);
        return inputData.map(item -> {
                    ringBuffer.put(item);
                    Decimal result = Decimal.ZERO;
                    for (int i = 1; i < ringBuffer.getSize(); i++) {
                        if (ringBuffer.get(i).minus(ringBuffer.get(i - 1)).isLessThan(Decimal.ZERO)) {
                            result = result.plus(ringBuffer.get(i - 1).minus(ringBuffer.get(i)));
                        }
                    }
                    return result;
                }
        );

    }
}
