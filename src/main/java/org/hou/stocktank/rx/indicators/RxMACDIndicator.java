package org.hou.stocktank.rx.indicators;

import io.reactivex.Flowable;
import org.hou.stocktank.rx.base.Decimal;


public class RxMACDIndicator implements RxIndicator<Decimal, Decimal> {
    private final int shortTimeFrame;
    private final int longTimeFrame;
    private RxEMAIndicator shortEmaIndicator;
    private RxEMAIndicator longEmaIndicator;

    public RxMACDIndicator(int shortTimeFrame, int longTimeFrame) {
        this.shortTimeFrame = shortTimeFrame;
        this.longTimeFrame = longTimeFrame;
        this.shortEmaIndicator = new RxEMAIndicator(shortTimeFrame);
        this.longEmaIndicator = new RxEMAIndicator(longTimeFrame);
    }

    @Override
    public Flowable<Decimal> calculate(Flowable<Decimal> inputData) {
        return shortEmaIndicator.calculate(inputData)
                .zipWith(longEmaIndicator.calculate(inputData), (shortE, longE) -> shortE.minus(longE));
    }

    public int getShortTimeFrame() {
        return shortTimeFrame;
    }

    public int getLongTimeFrame() {
        return longTimeFrame;
    }
}
