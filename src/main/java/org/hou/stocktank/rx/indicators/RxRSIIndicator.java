package org.hou.stocktank.rx.indicators;


import io.reactivex.Flowable;
import org.hou.stocktank.rx.base.Decimal;

import java.util.concurrent.atomic.AtomicInteger;

public class RxRSIIndicator implements RxIndicator<Decimal, Decimal> {
    private final int timeFrame;

    public RxRSIIndicator(int timeFrame) {
        this.timeFrame = timeFrame;
    }


    @Override
    public Flowable<Decimal> calculate(Flowable<Decimal> inputData) {
        Flowable<Decimal> averageLoss = new RxAverageLossIndicator(timeFrame).calculate(inputData);
        Flowable<Decimal> averageGain = new RxAverageGainIndicator(timeFrame).calculate(inputData);
        AtomicInteger counter = new AtomicInteger(0);
        return averageGain.zipWith(averageLoss, (gain, loss) -> {
            if (counter.getAndIncrement() == 0) {
                return Decimal.ZERO;
            }
            if (loss.isEqual(Decimal.ZERO)) {
                return Decimal.HUNDRED;
            }
            Decimal relativeStrength = gain.dividedBy(loss);
            Decimal ratio = Decimal.HUNDRED.dividedBy(Decimal.ONE.plus(relativeStrength));
            return Decimal.HUNDRED.minus(ratio);
        });
    }
}
