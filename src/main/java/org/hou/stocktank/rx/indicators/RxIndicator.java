package org.hou.stocktank.rx.indicators;

import io.reactivex.Flowable;

public interface RxIndicator<I, T> {
    Flowable<T> calculate(Flowable<I> inputData);
}
