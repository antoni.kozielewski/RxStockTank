package org.hou.stocktank.rx.base;

import org.joda.time.DateTime;

public class CollectibleTick extends Tick {

    public CollectibleTick(Tick tick){
        super(tick.getStockName(), tick.getOpenPrice(), tick.getClosePrice(), tick.getMinPrice(), tick.getMaxPrice(), tick.getVolume(), tick.getStartTimestamp(), tick.getEndTimestamp(), tick.getNumberOfTransactions());
    }

    public CollectibleTick(String ticker) {
        super(ticker, Decimal.NaN, Decimal.NaN, Decimal.NaN, Decimal.NaN, Decimal.ZERO, null, null, 0);
    }

    public CollectibleTick(String ticker, Decimal openPrice, Decimal closePrice, Decimal minPrice, Decimal maxPrice, Decimal volume, DateTime startTimestamp, DateTime endDateTime, Integer numberOfTransactions) {
        super(ticker, openPrice, closePrice, minPrice, maxPrice, volume, startTimestamp, endDateTime, numberOfTransactions);
    }

    public void collectTransaction(Transaction transaction) {
        if (this.openPrice.equals(Decimal.NaN)) {
            this.openPrice = transaction.getPrice();
        }
        if (this.getMinPrice().isGreaterThan(transaction.getPrice())) {
            this.minPrice = transaction.getPrice();
        }
        if (this.getMaxPrice().isLessThan(transaction.getPrice())) {
            this.maxPrice = transaction.getPrice();
        }
        this.closePrice = transaction.getPrice();
        if (this.startTimestamp == null) {
            this.startTimestamp = transaction.getTimestamp();
        }
        this.endTimestamp = transaction.getTimestamp();
        this.volume = this.volume.plus(transaction.getVolume());
        this.numberOfTransactions++;
    }

    public void collectTick(Tick tick) {
        if (this.openPrice.equals(Decimal.NaN)) {
            this.openPrice = tick.getOpenPrice();
        }
        if (this.getMinPrice().isGreaterThan(tick.getMinPrice())) {
            this.minPrice = tick.getMinPrice();
        }
        if (this.getMaxPrice().isLessThan(tick.getMaxPrice())) {
            this.maxPrice = tick.getMaxPrice();
        }
        this.closePrice = tick.getClosePrice();
        if (this.startTimestamp == null) {
            this.startTimestamp = tick.getStartTimestamp();
        }
        this.endTimestamp = tick.getEndTimestamp();
        this.volume = this.volume.plus(tick.getVolume());
        this.numberOfTransactions = this.numberOfTransactions + tick.getNumberOfTransactions();
    }

    public void resetToTransaction(Transaction transaction) {
        this.openPrice = transaction.getPrice();
        this.minPrice = transaction.getPrice();
        this.maxPrice = transaction.getPrice();
        this.closePrice = transaction.getPrice();
        this.startTimestamp = transaction.getTimestamp();
        this.endTimestamp = transaction.getTimestamp();
        this.volume = transaction.getVolume();
        this.numberOfTransactions = 1;
    }

    public void resetToTick(Tick tick) {
        this.openPrice = tick.getOpenPrice();
        this.minPrice = tick.getMinPrice();
        this.maxPrice = tick.getMaxPrice();
        this.closePrice = tick.getClosePrice();
        this.startTimestamp = tick.getStartTimestamp();
        this.endTimestamp = tick.getEndTimestamp();
        this.volume = tick.getVolume();
        this.numberOfTransactions = tick.getNumberOfTransactions();
    }

    public void setStartTimestamp(DateTime startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public void setEndTimestamp(DateTime endTimestamp) {
        this.endTimestamp = endTimestamp;
    }


    public String toString() {
        return super.toString() + " | number of transactions = " + numberOfTransactions;
    }

}
