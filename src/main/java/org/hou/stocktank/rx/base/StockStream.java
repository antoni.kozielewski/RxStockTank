package org.hou.stocktank.rx.base;


import io.reactivex.Flowable;

public class StockStream<T extends Tick> {
    private final String stockName;
    private final Flowable<T> data;
    private final TickInterval tickInterval;

    public StockStream(String stockName, Flowable<T> data, TickInterval tickInterval) {
        this.stockName = stockName;
        this.data = data;
        this.tickInterval = tickInterval;
    }

    public String getStockName() {
        return stockName;
    }

    public Flowable<T> getData() {
        return data;
    }

    public TickInterval getTickInterval() {
        return tickInterval;
    }
}
