package org.hou.stocktank.rx.base;


import org.joda.time.DateTime;


public class Tick {
    protected final String stockName;
    protected Decimal openPrice;
    protected Decimal closePrice;
    protected Decimal minPrice;
    protected Decimal maxPrice;
    protected Decimal volume;
    protected DateTime startTimestamp;
    protected DateTime endTimestamp;
    protected Integer numberOfTransactions;

    public Tick(String stockName, Decimal openPrice, Decimal closePrice, Decimal minPrice, Decimal maxPrice, Decimal volume, DateTime startTimestamp, DateTime endTimestamp, Integer numberOfTransactions) {
        this.stockName = stockName;
        this.openPrice = openPrice;
        this.closePrice = closePrice;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.volume = volume;
        this.startTimestamp = startTimestamp;
        this.endTimestamp = endTimestamp;
        this.numberOfTransactions = numberOfTransactions;
    }

    public String getStockName() {
        return stockName;
    }

    public Decimal getOpenPrice() {
        return openPrice;
    }

    public Decimal getClosePrice() {
        return closePrice;
    }

    public Decimal getMinPrice() {
        return minPrice;
    }

    public Decimal getMaxPrice() {
        return maxPrice;
    }

    /**
     * returns close price
     *
     * @return
     */
    public Decimal getPrice() {
        return closePrice;
    }

    /**
     * returns end of session timestamp
     *
     * @return
     */
    public DateTime getTimestamp() {
        return endTimestamp;
    }

    public Decimal getVolume() {
        return volume;
    }

    public DateTime getStartTimestamp() {
        return startTimestamp;
    }

    public DateTime getEndTimestamp() {
        return endTimestamp;
    }

    public Integer getNumberOfTransactions() {
        return numberOfTransactions;
    }

    public String toString() {
        return stockName + " | " + startTimestamp + "  " + endTimestamp + " | o " + openPrice + " | max " + maxPrice + " | min " + minPrice + " | c " + closePrice + " | vol = " + volume + " | transactions = " + numberOfTransactions;
    }
}
