package org.hou.stocktank.rx.base;

import org.joda.time.DateTime;

public class Order extends Transaction {
    protected Operation operation;
    protected Decimal commision;

    public Order(Operation operation, String ticker, DateTime timestamp, Decimal price, Decimal volume, Decimal commision) {
        super(ticker, timestamp, price, volume);
        this.operation = operation;
        this.commision = commision;
    }

    public Order(Operation operation, String ticker, DateTime timestamp, Decimal price, Decimal volume) {
        this(operation, ticker, timestamp, price, volume, Decimal.ZERO);
    }

    public Operation getOperation() {
        return operation;
    }

    public Decimal getCommision() {
        return commision;
    }

    public String toString() {
        return stockName + " | " + getTimestamp() + " | " + operation + " | volume = " + volume + " | price = " + getPrice() + " | commision = " + commision;
    }

}
