package org.hou.stocktank.rx.base;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class DecimalResults {
    public final static String TICK_CLOSE_PRICE = "closePrice";
    public final static String TICK_OPEN_PRICE = "openPrice";
    public final static String TICK_MIN_PRICE = "minPrice";
    public final static String TICK_MAX_PRICE = "maxPrice";
    private Tick tick;
    private Map<String, Decimal> values = new HashMap<>();

    public DecimalResults(Tick tick) {
        this.tick = tick;
        this.values.put(TICK_CLOSE_PRICE, tick.getClosePrice());
        this.values.put(TICK_OPEN_PRICE, tick.getOpenPrice());
        this.values.put(TICK_MIN_PRICE, tick.getMinPrice());
        this.values.put(TICK_MAX_PRICE, tick.getMaxPrice());
    }

    public Tick getTick() {
        return tick;
    }

    public DecimalResults addValue(String name, Decimal value) {
        values.put(name, value);
        return this;
    }

    public Decimal getValue(String name) {
        return values.get(name);
    }

    public boolean isGreaterThan(String a, String b) {
        return values.get(a).isGreaterThan(values.get(b));
    }

    public boolean isGreaterThanOrEqual(String a, String b) {
        return values.get(a).isGreaterThanOrEqual(values.get(b));
    }

    public boolean isLessThan(String a, String b) {
        return values.get(a).isLessThan(values.get(b));
    }

    public boolean isLessThanOrEqual(String a, String b) {
        return values.get(a).isLessThanOrEqual(values.get(b));
    }

    public boolean crossesDown(String a, String b, DecimalResults previous) {
        return (previous.isGreaterThan(a, b)) && (this.isLessThan(a, b));
    }

    public boolean crossesUp(String a, String b, DecimalResults previous) {
        return (previous.isLessThan(a, b)) && (this.isGreaterThan(a, b));
    }

    public String toString(){
        return tick + " ### " +  values.entrySet().stream().map(pair -> pair.getKey() +"->" + pair.getValue()).collect(Collectors.joining(" | "));
    }
}
