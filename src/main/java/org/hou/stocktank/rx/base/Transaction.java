package org.hou.stocktank.rx.base;

import org.joda.time.DateTime;

public class Transaction extends Tick {

    public Transaction(String ticker, DateTime timestamp, Decimal price, Decimal volume) {
        super(ticker, price, price, price, price, volume, timestamp, timestamp, 1);
    }

    public String toString() {
        return "TRANSACTION: " + stockName + " | " + getTimestamp() + " | price = " + getPrice() + " | vol = " + volume;
    }
}
