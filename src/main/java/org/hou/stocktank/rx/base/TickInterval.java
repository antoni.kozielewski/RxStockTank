package org.hou.stocktank.rx.base;

public enum TickInterval {
    MONTH,
    TWO_WEEKS,
    WEEK,
    DAY,
    HOUR,
    QUARTER,
    FIVE_MINUTES,
    MINUTE,
    TRANSACTION;
}
