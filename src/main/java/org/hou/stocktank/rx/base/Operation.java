package org.hou.stocktank.rx.base;


public enum Operation {
    SELL(-1),
    NO_ACTION(0),
    BUY(1),
    INFO(666);

    int value;

    Operation(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
