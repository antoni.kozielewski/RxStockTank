package org.hou.stocktank.rx.base;


public enum Trend {
    CHANGE_TO_BEARISH(-10),
    BEARISH(-1),
    CONSOLIDATION(0),
    BULLISH(1),
    CHANGE_TO_BULLISH(10);


    int value;

    Trend(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
