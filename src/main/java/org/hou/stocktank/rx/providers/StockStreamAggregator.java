package org.hou.stocktank.rx.providers;

import io.reactivex.Flowable;
import io.reactivex.functions.Function;
import org.hou.stocktank.rx.base.CollectibleTick;
import org.hou.stocktank.rx.base.StockStream;
import org.hou.stocktank.rx.base.Tick;
import org.hou.stocktank.rx.base.Transaction;
import org.joda.time.DateTime;
import org.reactivestreams.Publisher;

public class StockStreamAggregator {

    private static Flowable<Tick> getAggregatedStream(StockStream<Transaction> stockStream, Function<DateTime, DateTime> startTimestampFunction, Function<DateTime, DateTime> endTimestampFunction) {
        return stockStream
                .getData()
                .flatMap(new Function<Transaction, Publisher<Tick>>() {
                    CollectibleTick tickAccumulator = new CollectibleTick(stockStream.getStockName());

                    @Override
                    public Publisher<Tick> apply(Transaction transaction) throws Exception {
                        if (tickAccumulator.getStartTimestamp() == null) {
                            tickAccumulator.resetToTransaction(transaction);
                        } else if (startTimestampFunction.apply(tickAccumulator.getStartTimestamp()).equals(startTimestampFunction.apply(transaction.getTimestamp()))) {
                            tickAccumulator.collectTransaction(transaction);
                        } else {
                            CollectibleTick result = new CollectibleTick(tickAccumulator);
                            result.setStartTimestamp(startTimestampFunction.apply(result.getStartTimestamp()));
                            result.setEndTimestamp(endTimestampFunction.apply(result.getEndTimestamp()));
                            tickAccumulator.resetToTransaction(transaction);
                            return Flowable.just(result);
                        }
                        return Flowable.empty();
                    }
                })
                .publish()
                .refCount();
    }

    public static Flowable<Tick> getSecondAgregatedStream(StockStream<Transaction> stockStream) {
        return getAggregatedStream(stockStream,
                (dateTime -> dateTime.withMillisOfSecond(0)),
                (dateTime -> dateTime.withMillisOfSecond(0).plusSeconds(1)));
    }

    public static Flowable<Tick> getMinuteAggregatedStream(StockStream<Transaction> stockStream) {
        return getAggregatedStream(stockStream,
                (dateTime -> dateTime.withSecondOfMinute(0).withMillisOfSecond(0)),
                (dateTime -> dateTime.withSecondOfMinute(0).withMillisOfSecond(0).plusMinutes(1)));
    }

    public static Flowable<Tick> getHourAggregatedStream(StockStream<Transaction> stockStream) {
        return getAggregatedStream(stockStream,
                (dateTime -> dateTime.withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0)),
                (dateTime -> dateTime.withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0).plusHours(1)));
    }

    public static Flowable<Tick> getDayAggregatedStream(StockStream<Transaction> stockStream) {
        return getAggregatedStream(stockStream,
                (dateTime -> dateTime.withHourOfDay(9).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0)),
                (dateTime -> dateTime.withHourOfDay(17).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0)));
    }

}
