package org.hou.stocktank.rx.providers.bossa;

import io.reactivex.Flowable;
import org.hou.stocktank.rx.base.StockStream;
import org.hou.stocktank.rx.base.TickInterval;
import org.hou.stocktank.rx.base.Transaction;
import org.hou.stocktank.rx.providers.StocksStreamsProvider;
import org.hou.stocktank.rx.utils.NonLockingFileReaderPublisher;
import org.joda.time.DateTime;

import java.io.File;

public class BossaIntradayStreamsProvider implements StocksStreamsProvider<Transaction> {
    private final String staticaCSVPath;
    private final int newDataCheckPeriod;
    private final int queueMaxSize;
    private final int numberOfDaysBack;

    public BossaIntradayStreamsProvider(String staticaCSVPath, int numberOfDaysBack) {
        this(staticaCSVPath, numberOfDaysBack, 500, 50000);
    }

    public BossaIntradayStreamsProvider(String staticaCSVPath, int numberOfDaysBack, int newDataCheckPeriod, int queueMaxSize) {
        this.staticaCSVPath = staticaCSVPath;
        this.numberOfDaysBack = numberOfDaysBack;
        this.newDataCheckPeriod = newDataCheckPeriod;
        this.queueMaxSize = queueMaxSize;
    }

    @Override
    public StockStream<Transaction> getStockStream(String stockName) {
        return getStockTransactionStream(stockName, numberOfDaysBack);
    }

    private String prepareFilePath(String stockName) {
        return staticaCSVPath + File.separator + stockName + ".csv";
    }

    private StockStream<Transaction> getStockTransactionStream(String stockName, int numberOfDays) {
        Flowable<Transaction> data =
                Flowable.fromPublisher(new NonLockingFileReaderPublisher(prepareFilePath(stockName), newDataCheckPeriod, queueMaxSize, NonLockingFileReaderPublisher.EndOfLineChars.WINDOWS_EOL,
                        line -> {
                            Transaction transaction = BossaIntradayCSVParser.parseLineToTransaction(stockName, line);
                            if (transaction.getTimestamp().isAfter(DateTime.now().withMillisOfDay(0).minusDays(numberOfDays))) return true;
                            else return false;
                        }))
                        .map(line -> BossaIntradayCSVParser.parseLineToTransaction(stockName, line)) // to transaction
                        .publish()
                        .refCount();
        return new StockStream<>(stockName, data, TickInterval.TRANSACTION);
    }

}
