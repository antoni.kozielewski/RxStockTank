package org.hou.stocktank.rx.providers.bossa;

import org.joda.time.DateTime;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

public class BossaSingleFileDownloader {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(BossaSingleFileDownloader.class);
    private String dataURL  = "http://bossa.pl/pub/metastock/mstock/mstall.zip";
    private String dataPath;


    public void run() throws IOException {
        if (shouldDownload()) {
            download();
        }
    }

    private void download() {
        ReadableByteChannel in = null;
        try {
            logger.info("downloading : {}", dataURL);
            in = Channels.newChannel(new URL(dataURL).openStream());
            FileChannel out = new FileOutputStream(dataPath).getChannel();
            out.transferFrom(in, 0, Long.MAX_VALUE);
            out.close();
            logger.info("download finished : {}", dataURL);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Boolean shouldDownload() throws IOException {
        File f = new File(dataPath);
        if (!f.exists()) return true;
        ZipFile zipFile = null;
        try {
            zipFile = new ZipFile(f);
        } catch (ZipException e) {
            logger.info(" file corrupted - will be redownloaded.");
            f.delete();
            return true;
        }
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            ZipEntry entry = entries.nextElement();
            if (!entry.isDirectory()) {
                DateTime now = new DateTime();
                DateTime fileTime = new DateTime(entry.getTime());
                if (now.minusDays(1).isAfter(fileTime)) {
                    logger.info(" Data file is too old {} - need to download new one;", fileTime);
                    return true;
                }
                logger.info(" Data file is ok: {} ", fileTime);
                return false;
            }
        }
        logger.info(" There is no data file or some other shit happend. ");
        return true;
    }

    public String getDataURL() {
        return dataURL;
    }

    public void setDataURL(String url) {
        this.dataURL = url;
    }

    public String getDataPath() {
        return dataPath;
    }

    public void setDataPath(String dataPath) {
        this.dataPath = dataPath;
    }
}
