package org.hou.stocktank.rx.providers.bossa;

import org.hou.stocktank.rx.base.Decimal;
import org.hou.stocktank.rx.base.Transaction;
import org.joda.time.DateTime;

public class BossaIntradayCSVParser {

    public static Transaction parseLineToTransaction(String ticker, String line) {
        try {
            String[] data = line.split(",");
            int year = Integer.valueOf(data[0].substring(0, 2)) + 2000;
            int month = Integer.valueOf(data[0].substring(2, 4));
            int day = Integer.valueOf(data[0].substring(4));
            int hour = Integer.valueOf(data[1].substring(0, 2));
            int min = Integer.valueOf(data[1].substring(2, 4));
            int sec = Integer.valueOf(data[1].substring(4));
            Decimal price = Decimal.valueOf(data[2]);
            Decimal vol = Decimal.valueOf(data[3]);
            return new Transaction(ticker, new DateTime(year, month, day, hour, min, sec), price, vol);
        } catch (Exception e) {
            System.out.println("BossaIntradayCSVParser - parsing problem with line : '" + line + "'");
            throw new RuntimeException(e);
        }
    }
}
