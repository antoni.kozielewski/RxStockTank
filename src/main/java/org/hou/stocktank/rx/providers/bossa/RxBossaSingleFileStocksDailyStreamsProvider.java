package org.hou.stocktank.rx.providers.bossa;

import io.reactivex.Flowable;
import org.hou.stocktank.rx.base.Decimal;
import org.hou.stocktank.rx.base.StockStream;
import org.hou.stocktank.rx.base.Tick;
import org.hou.stocktank.rx.base.TickInterval;
import org.hou.stocktank.rx.providers.StocksStreamsProvider;
import org.hou.stocktank.rx.utils.IterableEnumeration;
import org.joda.time.DateTime;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;


public class RxBossaSingleFileStocksDailyStreamsProvider implements StocksStreamsProvider<Tick> {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(BossaSingleFileDownloader.class);
    private final Character ESCAPE_CHAR = '\\';
    private final Character QUOTE_CHAR = '"';
    private final Character SEPARATOR_CHAR = ',';
    private String dataPath;
    private String dataFileExtension = ".mst";

    private RxBossaSingleFileStocksDailyStreamsProvider() {

    }

    public RxBossaSingleFileStocksDailyStreamsProvider(String filePath) {
        super();
        this.dataPath = filePath;
    }

    @Override
    public StockStream<Tick> getStockStream(String stockName) {
        return new StockStream<>(stockName, getStream(stockName), TickInterval.DAY);
    }

    public Flowable<Tick> getStream(String stockName) {
        try {
            InputStream inputStream = getInputStream(stockName);

            return Flowable.using(
                    () -> {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(getInputStream(stockName)));
                        bufferedReader.readLine();
                        return bufferedReader;
                    },
                    reader -> Flowable.fromIterable(() -> reader.lines().iterator()),
                    reader -> reader.close())
                    .map(line -> parseLine(splitLine(line)));

        } catch (IOException e) {
            throw new RuntimeException(e); // TODO ?? or exception wrapper ?
        }
    }

    public Flowable<String> getStocksNames() {
        try {
            File f = new File(dataPath);
            ZipFile zipFile = new ZipFile(f);
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            return Flowable.fromIterable(new IterableEnumeration<>(entries))
                    .filter(entry -> !entry.isDirectory())
                    .map(entry -> entry.getName())
                    .map(name -> name.substring(0, name.indexOf(".")));
        } catch (IOException e) {
            throw new RuntimeException(e); // TODO maybe there should be exception wrapper
        }
    }

    public String getDataPath() {
        return dataPath;
    }

    public void setDataPath(String dataPath) {
        this.dataPath = dataPath;
    }

    public String getDataFileExtension() {
        return dataFileExtension;
    }

    public void setDataFileExtension(String dataFileExtension) {
        this.dataFileExtension = dataFileExtension;
    }

    private InputStream getInputStream(String stockName) throws IOException {
        File f = new File(dataPath);
        ZipFile zipFile = new ZipFile(f);
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            ZipEntry entry = entries.nextElement();
            if (!entry.isDirectory()) {
                if (entry.getName().toLowerCase().equals(stockName.toLowerCase() + dataFileExtension)) {
                    logger.debug("found : {}", stockName.toLowerCase() + dataFileExtension);
                    return zipFile.getInputStream(entry);
                }
            }
        }
        logger.error("file not found: {}/{}", dataPath, stockName.toLowerCase() + dataFileExtension);
        return null;
    }

    // format:
    //DTYYYYMMDD
    private DateTime parseDateTime(String dateField) {
        Integer year = Integer.valueOf(dateField.substring(0, 4));
        Integer month = Integer.valueOf(dateField.substring(4, 6));
        Integer day = Integer.valueOf(dateField.substring(6, 8));
        return new DateTime(year, month, day, 17, 0, 0, 0);
    }

    // format:
    //<TICKER>,<DTYYYYMMDD>,<OPEN>,<HIGH>,<LOW>,<CLOSE>,<VOL>
    private Tick parseLine(String[] line) {
        String ticker = line[0];
        DateTime endTimestamp = parseDateTime(line[1]);
        DateTime startTimestamp = endTimestamp.withHourOfDay(9);
        Decimal open = Decimal.valueOf(line[2]);
        Decimal high = Decimal.valueOf(line[3]);
        Decimal low = Decimal.valueOf(line[4]);
        Decimal close = Decimal.valueOf(line[5]);
        Decimal volume = Decimal.valueOf(line[6]);
        return new Tick(ticker, open, close, low, high, volume, startTimestamp, endTimestamp,0);
    }

    private Tick parseLine(String ticker, String line) {
        return parseLine(splitLine(line));
    }

    private boolean isEscapable(String nextLine, boolean inQuotes, int i) {
        return inQuotes && nextLine.length() > i + 1 && (nextLine.charAt(i + 1) == QUOTE_CHAR || nextLine.charAt(i + 1) == ESCAPE_CHAR);
    }

    private boolean isEscapedQuote(String nextLine, boolean inQuotes, int i) {
        return inQuotes && nextLine.length() > i + 1 && nextLine.charAt(i + 1) == QUOTE_CHAR;
    }

    private String[] splitLine(String line) {
        if (line == null) {
            return null;
        } else {
            ArrayList tokensOnThisLine = new ArrayList();
            StringBuilder sb = new StringBuilder(64);
            boolean inQuotes = false;

            do {
                if (inQuotes) {
                    return new String[]{};
                }

                for (int i = 0; i < line.length(); ++i) {
                    char c = line.charAt(i);
                    if (c == ESCAPE_CHAR) {
                        if (this.isEscapable(line, inQuotes, i)) {
                            sb.append(line.charAt(i + 1));
                            ++i;
                        } else {
                            ++i;
                        }
                    } else if (c == QUOTE_CHAR) {
                        if (this.isEscapedQuote(line, inQuotes, i)) {
                            sb.append(line.charAt(i + 1));
                            ++i;
                        } else {
                            inQuotes = !inQuotes;
                            if (i > 2 && line.charAt(i - 1) != SEPARATOR_CHAR && line.length() > i + 1 && line.charAt(i + 1) != SEPARATOR_CHAR) {
                                sb.append(c);
                            }
                        }
                    } else if (c == SEPARATOR_CHAR && !inQuotes) {
                        tokensOnThisLine.add(sb.toString());
                        sb = new StringBuilder(64);
                    } else {
                        sb.append(c);
                    }
                }
            } while (inQuotes);

            tokensOnThisLine.add(sb.toString());
            return (String[]) tokensOnThisLine.toArray(new String[0]);
        }
    }

}
