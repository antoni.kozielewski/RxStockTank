package org.hou.stocktank.rx.providers;


import org.hou.stocktank.rx.base.StockStream;
import org.hou.stocktank.rx.base.Tick;

/**
 * provide StockStreams
 */
public interface StocksStreamsProvider<T extends Tick> {

    StockStream<T> getStockStream(String stockName);

}
