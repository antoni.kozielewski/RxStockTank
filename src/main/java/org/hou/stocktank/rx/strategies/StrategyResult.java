package org.hou.stocktank.rx.strategies;


import org.hou.stocktank.rx.base.Operation;
import org.hou.stocktank.rx.base.Tick;

public class StrategyResult<T extends Operation> {
    private Tick tick;
    private T operation;
    private String description;

    public StrategyResult(Tick tick, T operation) {
        this.tick = tick;
        this.operation = operation;
        this.description = "";
    }

    public StrategyResult(Tick tick, T operation, String description) {
        this.tick = tick;
        this.operation = operation;
        this.description = description;
    }

    public Tick getTick() {
        return tick;
    }

    public T getOperation() {
        return operation;
    }

    public String getDescription() {
        return description;
    }

    public String toString() {
        return tick.toString() + " | " + operation.toString() + " | " + description;
    }
}
