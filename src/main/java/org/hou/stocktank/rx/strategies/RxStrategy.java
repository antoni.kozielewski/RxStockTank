package org.hou.stocktank.rx.strategies;

import io.reactivex.Flowable;


public interface RxStrategy {

    Flowable<StrategyResult> check();
}
