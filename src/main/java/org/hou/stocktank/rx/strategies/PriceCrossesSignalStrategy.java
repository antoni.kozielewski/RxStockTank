package org.hou.stocktank.rx.strategies;

import io.reactivex.Flowable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import org.hou.stocktank.rx.base.Decimal;
import org.hou.stocktank.rx.base.Operation;
import org.hou.stocktank.rx.base.Tick;
import org.reactivestreams.Publisher;

public class PriceCrossesSignalStrategy implements RxStrategy {
    protected Flowable<Tick> source;
    protected Flowable<Decimal> signal;

    private class InternalPair {
        private Tick tick;
        private Decimal signal;

        public InternalPair(Tick tick, Decimal signal) {
            this.tick = tick;
            this.signal = signal;
        }

        public Tick getTick() {
            return tick;
        }

        public Decimal getSignal() {
            return signal;
        }
    }

    public PriceCrossesSignalStrategy(Flowable<Tick> source, Flowable<Decimal> signal) {
        this.source = source;
        this.signal = signal;
    }

    @Override
    public Flowable<StrategyResult> check() {
        return Flowable
                .combineLatest(source, signal, InternalPair::new)
                .flatMap(new Function<InternalPair, Publisher<StrategyResult>>() {
                    private InternalPair previous;

                    @Override
                    public Publisher<StrategyResult> apply(@NonNull InternalPair current) throws Exception {
                        if (previous == null) {
                            previous = current;
                            return Flowable.just(new StrategyResult(current.getTick(), Operation.NO_ACTION));
                        }
                        // this tick crosses
                        if (current.getTick().getOpenPrice().isGreaterThan(current.signal) &&
                                current.getTick().getClosePrice().isLessThan(current.signal)) {
                            // current tick crosses signal down
                            previous = current;
                            return Flowable.just(new StrategyResult(current.getTick(), Operation.SELL));
                        } else if (current.getTick().getOpenPrice().isLessThan(current.signal) &&
                                current.getTick().getClosePrice().isGreaterThan(current.signal)) {
                            // current tick crosses signal up
                            previous = current;
                            return Flowable.just(new StrategyResult(current.getTick(), Operation.BUY));
                        } else if (previous.getTick().getMaxPrice().isGreaterThan(previous.signal) &&
                                current.getTick().getClosePrice().isLessThan(current.signal)) {
                            // previous to current crosses signal down
                            previous = current;
                            return Flowable.just(new StrategyResult(current.getTick(), Operation.SELL));
                        } else if (previous.getTick().getMinPrice().isLessThan(previous.signal) &&
                                current.getTick().getClosePrice().isGreaterThan(current.signal)) {
                            // previous to current crosses signal down
                            previous = current;
                            return Flowable.just(new StrategyResult(current.getTick(), Operation.BUY));
                        } else {
                            previous = current;
                            return Flowable.just(new StrategyResult(current.getTick(), Operation.NO_ACTION));
                        }
                    }
                });
    }

}
