package org.hou.stocktank.rx.strategies;


import io.reactivex.Flowable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import org.hou.stocktank.rx.base.Decimal;
import org.hou.stocktank.rx.base.Operation;
import org.hou.stocktank.rx.base.Tick;
import org.reactivestreams.Publisher;

public class ACrossesBStrategy implements RxStrategy {
    private Flowable<Decimal> a;
    private Flowable<Decimal> b;
    private Flowable<Tick> source;

    private class InternalTripple {
        private Tick tick;
        private Decimal a;
        private Decimal b;

        public InternalTripple(Tick tick, Decimal a, Decimal b) {
            this.tick = tick;
            this.a = a;
            this.b = b;
        }

        public Tick getTick() {
            return tick;
        }

        public Decimal getA() {
            return a;
        }

        public Decimal getB() {
            return b;
        }
    }

    public ACrossesBStrategy(Flowable<Tick> source, Flowable<Decimal> a, Flowable<Decimal> b) {
        this.source = source;
        this.a = a;
        this.b = b;
    }

    @Override
    public Flowable<StrategyResult> check() {
        return Flowable.combineLatest(source, a, b, InternalTripple::new)
                .flatMap(new Function<InternalTripple, Publisher<StrategyResult>>() {
                             private InternalTripple previous;

                             @Override
                             public Publisher<StrategyResult> apply(@NonNull InternalTripple current) throws Exception {
                                 if (previous == null) {
                                     previous = current;
                                     return Flowable.just(new StrategyResult<>(current.tick, Operation.NO_ACTION, a + " 'A' level "));
                                 } else {
                                     if (previous.getA().isLessThan(previous.getB()) && (current.getA().isGreaterThan(current.getB()))) {
                                         // A crosses B up
                                         previous = current;
                                         return Flowable.just(new StrategyResult<>(current.tick, Operation.BUY));
                                     } else if (previous.getA().isGreaterThan(previous.getB()) && (current.getA().isLessThan(current.getB()))) {
                                         // A crosses down B
                                         previous = current;
                                         return Flowable.just(new StrategyResult<>(current.tick, Operation.SELL));
                                     }
                                     previous = current;
                                     return Flowable.just(new StrategyResult<>(current.tick, Operation.NO_ACTION,
                                             "PREVIOUS A / B:: " + previous.getA() +
                                             " / " + previous.getB() +
                                             " |||  CURRENT A / B:: " + current.getA() +
                                             " / " + current.getB()));
                                 }
                             }
                         }
                );
    }

}
