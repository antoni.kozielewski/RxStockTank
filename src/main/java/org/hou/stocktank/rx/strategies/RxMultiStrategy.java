package org.hou.stocktank.rx.strategies;


import io.reactivex.Flowable;

public interface RxMultiStrategy {

    Flowable<Flowable<StrategyResult>> check();
}
