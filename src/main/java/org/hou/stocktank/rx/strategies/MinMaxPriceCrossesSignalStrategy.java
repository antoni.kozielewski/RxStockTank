package org.hou.stocktank.rx.strategies;

import io.reactivex.Flowable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import org.hou.stocktank.rx.base.Decimal;
import org.hou.stocktank.rx.base.Tick;
import org.reactivestreams.Publisher;

public class MinMaxPriceCrossesSignalStrategy implements RxStrategy {
    private Flowable<Decimal> signal;
    private Flowable<Tick> source;

    private class InternalPair {
        private Tick tick;
        private Decimal signalValue;

        public InternalPair(Tick tick, Decimal signalValue) {
            this.tick = tick;
            this.signalValue = signalValue;
        }

        public Tick getTick() {
            return tick;
        }

        public Decimal getSignalValue() {
            return signalValue;
        }
    }

    public MinMaxPriceCrossesSignalStrategy(Flowable<Tick> source, Flowable<Decimal> signal) {
        this.signal = signal;
        this.source = source;
    }

    @Override
    public Flowable<StrategyResult> check() {
        return Flowable
                .combineLatest(source, signal, InternalPair::new)
                .flatMap(new Function<InternalPair, Publisher<StrategyResult>>() {
                    private InternalPair previous;

                    @Override
                    public Publisher<StrategyResult> apply(@NonNull InternalPair internalPair) throws Exception {
                        return null;
                    }

                });
    }

}
