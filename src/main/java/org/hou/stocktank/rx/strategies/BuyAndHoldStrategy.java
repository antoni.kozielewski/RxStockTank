package org.hou.stocktank.rx.strategies;

import io.reactivex.Flowable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import org.hou.stocktank.rx.base.Operation;
import org.hou.stocktank.rx.base.Tick;
import org.reactivestreams.Publisher;

import java.util.concurrent.atomic.AtomicBoolean;

public class BuyAndHoldStrategy implements RxStrategy {
    private Flowable<Tick> source;

    public BuyAndHoldStrategy(Flowable<Tick> source) {
        this.source = source;
    }

    @Override
    public Flowable<StrategyResult> check() {
        return source.flatMap(new Function<Tick, Publisher<StrategyResult>>() {
                                  private AtomicBoolean bought = new AtomicBoolean(false);

                                  @Override
                                  public Publisher<StrategyResult> apply(@NonNull Tick tick) throws Exception {
                                      if (bought.get()) {
                                          return Flowable.empty();
                                      } else {
                                          return Flowable.just(new StrategyResult<>(tick, Operation.BUY));
                                      }
                                  }
                              }
        );
    }

}
