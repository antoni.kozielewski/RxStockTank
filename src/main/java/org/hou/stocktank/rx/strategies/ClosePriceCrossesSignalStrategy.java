package org.hou.stocktank.rx.strategies;


import io.reactivex.Flowable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import org.hou.stocktank.rx.base.Decimal;
import org.hou.stocktank.rx.base.Operation;
import org.hou.stocktank.rx.base.Tick;
import org.reactivestreams.Publisher;

public class ClosePriceCrossesSignalStrategy implements RxStrategy {
    private Flowable<Decimal> signal;
    private Flowable<Tick> source;

    private class InternalPair {
        private Tick tick;
        private Decimal signalValue;

        public InternalPair(Tick tick, Decimal signalValue) {
            this.tick = tick;
            this.signalValue = signalValue;
        }

        public Tick getTick() {
            return tick;
        }

        public Decimal getSignalValue() {
            return signalValue;
        }
    }

    public ClosePriceCrossesSignalStrategy(Flowable<Tick> source, Flowable<Decimal> signal) {
        this.source = source;
        this.signal = signal;
    }

    @Override
    public Flowable<StrategyResult> check() {
        return Flowable.combineLatest(source, signal, InternalPair::new)
                .flatMap(new Function<InternalPair, Publisher<StrategyResult>>() {
                             private InternalPair previous;

                             @Override
                             public Publisher<StrategyResult> apply(@NonNull InternalPair current) throws Exception {
                                 if (previous == null) {
                                     previous = current;
                                     return Flowable.just(new StrategyResult<>(current.getTick(), Operation.NO_ACTION, signal + " sma level "));
                                 } else {
                                     if (previous.getTick().getClosePrice().isLessThan(previous.getSignalValue()) && (current.getTick().getClosePrice().isGreaterThan(current.getSignalValue()))) {
                                         // close price crosses up signalIndicator
                                         previous = current;
                                         return Flowable.just(new StrategyResult<>(current.getTick(), Operation.BUY));
                                     } else if (previous.getTick().getClosePrice().isGreaterThan(previous.getSignalValue()) && (current.getTick().getClosePrice().isLessThan(current.getSignalValue()))) {
                                         // close price crosses down signalIndicator
                                         previous = current;
                                         return Flowable.just(new StrategyResult<>(current.getTick(), Operation.SELL));
                                     }
                                     previous = current;
                                     return Flowable.just(new StrategyResult<>(current.getTick(), Operation.NO_ACTION,
                                             "PREVIOUS :: " + previous.getTick().getClosePrice() +
                                             " / " + previous.getSignalValue() +
                                             " |||  CURRENT :: " + current.getTick().getClosePrice() +
                                             " / " + current.getSignalValue()));
                                 }
                             }
                         }
                );
    }

}
