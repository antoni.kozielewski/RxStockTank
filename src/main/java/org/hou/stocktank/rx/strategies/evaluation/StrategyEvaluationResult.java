package org.hou.stocktank.rx.strategies.evaluation;


import org.hou.stocktank.rx.base.Decimal;
import org.hou.stocktank.rx.base.Order;
import org.hou.stocktank.rx.strategies.StrategyResult;
import org.joda.time.DateTime;

import java.util.List;
import java.util.Optional;

public class StrategyEvaluationResult {
    private DateTime dateTime;
    private StrategyResult signal;
    private Decimal startCash;
    private Decimal startVolume;
    private Decimal startAvgPrice;
    private Decimal finalCash;
    private Decimal finalVolume;
    private Decimal finalStockPrice;
    private List<Order> orders;

    public StrategyEvaluationResult(DateTime dateTime, StrategyResult signal, Decimal startCash, Decimal startVolume, Decimal startAvgPrice, Decimal finalCash, Decimal finalVolume, Decimal finalStockPrice, List<Order> orders) {
        this.dateTime = dateTime;
        this.signal = signal;
        this.startCash = startCash;
        this.startVolume = startVolume;
        this.startAvgPrice = startAvgPrice;
        this.finalCash = finalCash;
        this.finalVolume = finalVolume;
        this.finalStockPrice = finalStockPrice;
        this.orders = orders;
    }

    public StrategyResult getSignal() {
        return signal;
    }

    public DateTime getDateTime() {
        return dateTime;
    }

    public Decimal getStartCash() {
        return startCash;
    }

    public Decimal getStartValue() {
        if (startAvgPrice.isNaN()) {
            return startCash;
        }
        return startCash.plus((startAvgPrice.multipliedBy(startVolume)));
    }

    public Decimal getStartVolume() {
        return startVolume;
    }

    public Decimal getStartAvgPrice() {
        return startAvgPrice;
    }

    public Decimal getFinalCash() {
        return finalCash;
    }

    public Decimal getFinalVolume() {
        return finalVolume;
    }

    public Decimal getFinalStockPrice() {
        return finalStockPrice;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public Decimal getFinalNetValue() {
        return finalCash.plus(finalStockPrice.multipliedBy(finalVolume));
    }

    public Decimal getFinalGrossValue() {
        return (finalCash.minus(getTotalCosts())).plus(finalStockPrice.multipliedBy(finalVolume));
    }

    public Decimal getTotalCosts() {
        Optional<Decimal> sum = orders.stream().map(o -> o.getCommision()).reduce(Decimal::plus);
        return sum.isPresent() ? sum.get().round(2) : Decimal.ZERO;
    }

    public Decimal getFinalNetProfitRatio() {
        return (((getFinalNetValue().dividedBy(getStartValue())).multipliedBy(Decimal.HUNDRED)).round(2)).minus(Decimal.HUNDRED);
    }

    public Decimal getFinalGrossProfitRatio() {
        return (((getFinalGrossValue().dividedBy(getStartValue())).multipliedBy(Decimal.HUNDRED)).round(2)).minus(Decimal.HUNDRED);
    }

    public String getFullDescription() {
        StringBuilder builder = new StringBuilder("-------------------------------------------------------------\n\r");
        builder.append("date time                 : " + dateTime + "\n\r");
        builder.append("start budget              : " + startCash + "\n\r");
        builder.append("start stock volume        : " + startVolume + "\n\r");
        builder.append("start stock avg buy price : " + startAvgPrice + "\n\r");
        builder.append("\n\r");
        builder.append("final account             : " + finalCash + "\n\r");
        builder.append("final volume              : " + finalVolume + "\n\r");
        builder.append("final stock price         : " + finalStockPrice + "\n\r");
        builder.append("\n\r");
        builder.append(" nett profit ratio  (no commisions)   : " + getFinalNetProfitRatio() + "%\n\r");
        builder.append(" gross profit ratio (with commisions) : " + getFinalGrossProfitRatio() + "%\n\r");
        builder.append("\n\r");
        builder.append("orders [" + orders.size() + "]:\n\r");
        int i = 0;
        for (Order order : orders) {
            i++;
            builder.append(i + " " + order + "\n\r");
        }
        builder.append("-------------------------------------------------------------\n\r");
        return builder.toString();
    }

    public String toString() {
        return dateTime + " | " + signal.getOperation() + " | profits " + getFinalNetProfitRatio() + "% :: " + getFinalGrossProfitRatio() + "% | final net level : " + getFinalNetValue() + " | final volume " + getFinalVolume() + " | operations = " + orders.size() + " | commisions : " + getTotalCosts();
    }

}
