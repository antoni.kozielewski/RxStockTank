package org.hou.stocktank.rx.strategies;


import io.reactivex.Flowable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import org.hou.stocktank.rx.base.Operation;
import org.reactivestreams.Publisher;

public class TimeTresholdStrategyFilter implements RxStrategy {
    RxStrategy strategy;
    int timeTresholdInMinutes;

    public TimeTresholdStrategyFilter(RxStrategy strategy, int timeTresholdInMinutes) {
        this.strategy = strategy;
        this.timeTresholdInMinutes = timeTresholdInMinutes;
    }

    @Override
    public Flowable<StrategyResult> check() {
        return strategy.check()
                .flatMap(new Function<StrategyResult, Publisher<StrategyResult>>() {
                    private StrategyResult lastSignal;

                    @Override
                    public Publisher<StrategyResult> apply(@NonNull StrategyResult signal) throws Exception {
                        if (signal.getOperation() == Operation.BUY || signal.getOperation() == Operation.SELL) {
                            lastSignal = signal;
                        }
                        if (lastSignal != null) {
                            if (signal.getTick().getStartTimestamp().minusMinutes(timeTresholdInMinutes).isAfter(lastSignal.getTick().getStartTimestamp())) {
                                Flowable<StrategyResult> result = Flowable.just(new StrategyResult(signal.getTick(), lastSignal.getOperation(), " signalIndicator from : " + lastSignal.getTick().getStartTimestamp()));
                                lastSignal = null;
                                return result;
                            }
                        }
                        return Flowable.just(signal);
                    }
                });
    }
}
