package org.hou.stocktank.rx.strategies.evaluation;

import io.reactivex.Flowable;
import org.hou.stocktank.rx.base.AtomicDecimal;
import org.hou.stocktank.rx.base.Decimal;
import org.hou.stocktank.rx.base.Operation;
import org.hou.stocktank.rx.base.Order;
import org.hou.stocktank.rx.strategies.RxStrategy;
import org.joda.time.DateTime;

import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import static org.hou.stocktank.rx.base.Operation.BUY;
import static org.hou.stocktank.rx.base.Operation.SELL;

public class StrategyEvaluator {
    private RxStrategy strategy;
    private DateTime strategyStartDateTime = null;
    private AccountState startAccountState = new AccountState(Decimal.valueOf(10000), new Stock(Decimal.ZERO, Decimal.NaN));

    public class AccountState {
        private Decimal cash;
        private Stock stock;

        public AccountState(Decimal cash, Stock stock) {
            this.cash = cash;
            this.stock = stock;
        }

        public Decimal getCash() {
            return cash;
        }

        public Stock getStock() {
            return stock;
        }
    }

    public class Stock {
        protected Decimal volume;
        protected Decimal price;

        public Stock(Decimal volume, Decimal price) {
            this.volume = volume;
            this.price = price;
        }

        public Decimal getVolume() {
            return volume;
        }

        public Decimal getPrice() {
            return price;
        }
    }

    public StrategyEvaluator(RxStrategy strategy) {
        this.strategy = strategy;
    }

    public StrategyEvaluator withStartAccountState(Decimal money) {
        this.startAccountState = new AccountState(money, new Stock(Decimal.ZERO, Decimal.NaN));
        return this;
    }

    public StrategyEvaluator withStartAccountState(Decimal money, Decimal volume, Decimal avgBuyPrice) {
        this.startAccountState = new AccountState(money, new Stock(volume, avgBuyPrice));
        return this;
    }

    public StrategyEvaluator withStartDateTime(DateTime dateTime) {
        this.strategyStartDateTime = dateTime;
        return this;
    }

    private Decimal getCostOfOperation(Operation operation, Decimal price, Decimal volume) {
        return Decimal.valueOf(0.0039).multipliedBy(price.multipliedBy(volume)).round(2); // FIXME MBank commisstion hardcoded
    }

    public Flowable<StrategyEvaluationResult> evaluate() {
        List<Order> orders = new CopyOnWriteArrayList<>();
        AtomicDecimal money = new AtomicDecimal(startAccountState.cash);
        AtomicReference<StrategyEvaluationResult> lastResult = new AtomicReference<>();
        Queue<Stock> stocks = new ConcurrentLinkedQueue<>();
        if (startAccountState.stock.volume.isGreaterThan(Decimal.ZERO)) {
            stocks.add(startAccountState.stock);
        }

        AtomicInteger buyOrSell = new AtomicInteger(0);
        return strategy.check()
                .filter(signal -> {
                    if ((strategyStartDateTime == null) || ((strategyStartDateTime != null) && (signal.getTick().getStartTimestamp().isAfter(strategyStartDateTime)))) {
                        return true;
                    }
                    return false;
                })
                .flatMap(signal -> {
                            if ((signal.getOperation() == BUY)&&(buyOrSell.get()<=0)) {
                                if (money.get().isGreaterThan(signal.getTick().getClosePrice())) {
                                    // buy
                                    Decimal volume = Decimal.valueOf(Math.floor(money.get().dividedBy(signal.getTick().getClosePrice()).toDouble()));
                                    stocks.add(new Stock(volume, signal.getTick().getClosePrice()));
                                    money.set(money.get().minus(signal.getTick().getClosePrice().multipliedBy(volume)));
                                    orders.add(new Order(BUY, signal.getTick().getStockName(), signal.getTick().getStartTimestamp(), signal.getTick().getClosePrice(), volume, getCostOfOperation(BUY, signal.getTick().getClosePrice(), volume)));
                                    Optional<Stock> finalStock = stocks.stream()
                                            .reduce((acc, stock) -> new Stock(
                                                    acc.volume.plus(stock.volume),
                                                    (acc.price.multipliedBy(acc.volume).plus(stock.price.multipliedBy(stock.volume))).dividedBy(acc.volume.plus(stock.volume))));
                                    StrategyEvaluationResult result = null;
                                    if (finalStock.isPresent()) {
                                        result = new StrategyEvaluationResult(orders.get(orders.size() - 1).getTimestamp(), signal, startAccountState.cash, startAccountState.stock.volume, startAccountState.stock.price, money.get(), finalStock.get().volume, finalStock.get().price, orders);
                                    } else {
                                        result = new StrategyEvaluationResult(orders.get(orders.size() - 1).getTimestamp(), signal, startAccountState.cash, startAccountState.stock.volume, startAccountState.stock.price, money.get(), Decimal.ZERO, Decimal.NaN, orders);
                                    }
                                    lastResult.set(result);
                                    buyOrSell.set(1);
                                    return Flowable.just(result);
                                }
                            } else if ((signal.getOperation() == SELL)&&(buyOrSell.get() >= 0)) {
                                if (stocks.size() > 0) {
                                    while (stocks.size() > 0) {
                                        Stock stock = stocks.poll();
                                        money.addAndGet(signal.getTick().getClosePrice().multipliedBy(stock.volume));
                                        orders.add(new Order(SELL, signal.getTick().getStockName(), signal.getTick().getStartTimestamp(), signal.getTick().getMinPrice(), stock.volume, getCostOfOperation(SELL, signal.getTick().getMinPrice(), stock.volume)));
                                        Optional<Stock> finalStock = stocks.stream()
                                                .reduce((acc, st) -> new Stock(
                                                        acc.volume.plus(st.volume),
                                                        (acc.price.multipliedBy(acc.volume).plus(st.price.multipliedBy(st.volume))).dividedBy(acc.volume.plus(st.volume))));
                                        StrategyEvaluationResult result = null;
                                        if (finalStock.isPresent()) {
                                            result = new StrategyEvaluationResult(orders.get(orders.size() - 1).getTimestamp(), signal, startAccountState.cash, startAccountState.stock.volume, startAccountState.stock.price, money.get(), finalStock.get().volume, finalStock.get().price, orders);
                                        } else {
                                            result = new StrategyEvaluationResult(orders.get(orders.size() - 1).getTimestamp(), signal, startAccountState.cash, startAccountState.stock.volume, startAccountState.stock.price, money.get(), Decimal.ZERO, Decimal.ZERO, orders);
                                        }
                                        lastResult.set(result);
                                        buyOrSell.set(-1);
                                        return Flowable.just(result);
                                    }
                                }
                            }

                            if (lastResult.get() != null){
                                return Flowable.just(new StrategyEvaluationResult(
                                        signal.getTick().getStartTimestamp(),
                                        signal,
                                        lastResult.get().getStartCash(),
                                        lastResult.get().getStartVolume(),
                                        lastResult.get().getStartAvgPrice(),
                                        lastResult.get().getFinalCash(),
                                        lastResult.get().getFinalVolume(),
                                        signal.getTick().getMinPrice(),
                                        lastResult.get().getOrders()
                                ));
                            }
                            return Flowable.empty();
                        }
                );
    }
}
