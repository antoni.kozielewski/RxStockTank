package org.hou.stocktank.rx.indicators;


import com.opencsv.CSVReader;
import org.hou.stocktank.rx.providers.bossa.BossaSingleFileDownloader;
import org.slf4j.LoggerFactory;
import org.ta4j.core.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class TA4JBossaSingleFileDataProviderService {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(BossaSingleFileDownloader.class);
    private String dataPath;
    private String dataFileExtension = ".mst";

    public TA4JBossaSingleFileDataProviderService(){
        super();
    }

    private InputStream getStream(String stockName) throws IOException {
        File f = new File(dataPath);
        ZipFile zipFile = new ZipFile(f);
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            ZipEntry entry = entries.nextElement();
            if (!entry.isDirectory()) {
                if (entry.getName().toLowerCase().equals(stockName.toLowerCase() + dataFileExtension)) {
                    logger.debug("found : {}", stockName.toLowerCase() + dataFileExtension);
                    return zipFile.getInputStream(entry);
                }
            }
        }
        logger.error("file not found: {}/{}", dataPath, stockName.toLowerCase() + dataFileExtension);
        return null;
    }

    // format:
    //DTYYYYMMDD
    private static ZonedDateTime parseDateTime(String dateField) {
        Integer year = Integer.valueOf(dateField.substring(0, 4));
        Integer month = Integer.valueOf(dateField.substring(4, 6));
        Integer day = Integer.valueOf(dateField.substring(6, 8));
        return ZonedDateTime.of(year, month, day, 0, 0, 0, 0, ZoneId.systemDefault());
    }

    // format:
    //<TICKER>,<DTYYYYMMDD>,<OPEN>,<HIGH>,<LOW>,<CLOSE>,<VOL>
    private static Tick parseLine(String[] line) {
        ZonedDateTime date = parseDateTime(line[1]);
        Decimal open = Decimal.valueOf(line[2]);
        Decimal high = Decimal.valueOf(line[3]);
        Decimal low = Decimal.valueOf(line[4]);
        Decimal close = Decimal.valueOf(line[5]);
        Decimal volume = Decimal.valueOf(line[6]);
        return new BaseTick(Duration.ofHours(8), date.withHour(17), open, high, low, close, volume);
    }

    public TimeSeries load(String stockName) {
        List<Tick> ticks = new ArrayList<Tick>();
        try {
            InputStream stream = getStream(stockName);
            if (stream != null) {
                CSVReader csvReader = new CSVReader(new InputStreamReader(stream, Charset.forName("UTF-8")), ',', '"', 1);
                String[] line;
                while ((line = csvReader.readNext()) != null) {
                    ticks.add(parseLine(line));
                }
            } else {
                return new BaseTimeSeries(new ArrayList<Tick>());
            }
        } catch (IOException ioe) {
            logger.error("Unable to load ticks from CSV", ioe);
        } catch (NumberFormatException nfe) {
            logger.error("Error while parsing level", nfe);
        }
        return new BaseTimeSeries(stockName, ticks);
    }

    public String getDataPath() {
        return dataPath;
    }

    public String getDataFileExtension() {
        return dataFileExtension;
    }

    public void setDataPath(String dataPath) {
        this.dataPath = dataPath;
    }

    public void setDataFileExtension(String dataFileExtension) {
        this.dataFileExtension = dataFileExtension;
    }
}
