package org.hou.stocktank.rx.indicators;

import org.hou.stocktank.indicators.ParabolicSARIndicator;
import org.hou.stocktank.rx.providers.bossa.RxBossaSingleFileStocksDailyStreamsProvider;
import org.ta4j.core.Decimal;
import org.ta4j.core.TimeSeries;

import java.util.concurrent.atomic.AtomicInteger;

public class TA4JandRxIndicatorComparationTool {

    public static void main(String[] args) {
        String filename = "src/test/resources/mstall.zip";
        RxBossaSingleFileStocksDailyStreamsProvider rxProvider = new RxBossaSingleFileStocksDailyStreamsProvider(filename);
        TA4JBossaSingleFileDataProviderService ta4jProvider = new TA4JBossaSingleFileDataProviderService();
        ta4jProvider.setDataPath(filename);

        String stockName = "CDPROJEKT";

        TimeSeries timeSeries = ta4jProvider.load(stockName);
        org.hou.stocktank.indicators.ParabolicSARIndicator ta4jIndicator = new ParabolicSARIndicator(timeSeries, 0.02, 0.02, 0.2);
        RxSARIndicator rxIndicator = new RxSARIndicator(0.02, 0.02, 0.2);

        AtomicInteger index = new AtomicInteger(0);
        rxProvider
                .getStream(stockName)
                .zipWith(rxIndicator.calculate(rxProvider.getStream(stockName)),
                        (tick, rxValue) -> {
                            Decimal ta4jValue = ta4jIndicator.getValue(index.getAndIncrement());
                            return tick + " ||| rxValue = " + rxValue + " <> ta4jValue = " + ta4jValue;
                        }
                )
                .subscribe(System.out::println);

    }
}
