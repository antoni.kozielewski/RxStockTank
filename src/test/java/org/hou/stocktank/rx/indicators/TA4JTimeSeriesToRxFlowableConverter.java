package org.hou.stocktank.rx.indicators;


import io.reactivex.Flowable;
import org.hou.stocktank.rx.base.Decimal;
import org.hou.stocktank.rx.base.Tick;
import org.joda.time.DateTime;
import org.ta4j.core.TimeSeries;

public class TA4JTimeSeriesToRxFlowableConverter {

    public static Flowable<Tick> convertTimeSeries2FlowableOfTick(String ticker, TimeSeries timeSeries) {
        return Flowable.fromIterable(timeSeries.getTickData())
                .map(ta4jTick ->
                        new Tick(
                                ticker,
                                Decimal.valueOf(ta4jTick.getOpenPrice().toDouble()),
                                Decimal.valueOf(ta4jTick.getClosePrice().toDouble()),
                                Decimal.valueOf(ta4jTick.getMinPrice().toDouble()),
                                Decimal.valueOf(ta4jTick.getMaxPrice().toDouble()),
                                Decimal.valueOf(ta4jTick.getVolume().toString()),
                                new DateTime(ta4jTick.getBeginTime()),
                                new DateTime(ta4jTick.getEndTime()),
                                -1)
                );
    }
}
