package org.hou.stocktank.rx.indicators;


import org.hou.stocktank.indicators.ParabolicSARIndicator;
import org.hou.stocktank.rx.base.Tick;
import org.hou.stocktank.rx.providers.bossa.BossaSingleFileDownloader;
import org.ta4j.core.Decimal;
import org.ta4j.core.indicators.*;
import org.ta4j.core.indicators.helpers.*;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;


public class TestIndicators {

    public static void main(String[] args) throws IOException {
        System.out.println("---------- START -------------");
        String filename = "src/test/resources/mstall.zip";
        File file = new File(filename);
        if (!file.exists()) {
            System.out.println(" DOWNLOADING DATA FILE ");
            BossaSingleFileDownloader downloader = new BossaSingleFileDownloader();
            downloader.setDataPath(filename);
            downloader.run();
        }
        List<String> stocks = Arrays.asList("JUJUBEE", "CDPROJEKT", "KGHM", "WIG20", "AMICA", "KRUK", "CEZ", "ATAL", "PGE", "TAURONPE", "ENEA", "VIVID", "MBANK", "FARM51", "NEUCA", "WIELTON", "11BIT");
        TA4jvsRXTester tester = new TA4jvsRXTester(stocks, filename);

        // compare function - compare TA4J Decimal with our Decimal
        BiFunction<org.ta4j.core.Decimal, org.hou.stocktank.rx.base.Decimal, Boolean> decimalDecimalComparationFunction = new BiFunction<Decimal, org.hou.stocktank.rx.base.Decimal, Boolean>() {
            @Override
            public Boolean apply(Decimal ta4jValue, org.hou.stocktank.rx.base.Decimal rxValue) {
                if (ta4jValue.isNaN() && rxValue.isNaN()) return true;
                else return ta4jValue.toDouble() == rxValue.toDouble();
            }
        };

        /**
         * --------------------------------------------------------------------------------------------------------
         * if you want to test your indicators please add that below
         * --------------------------------------------------------------------------------------------------------
          */

        // example
        tester.test(
                timeSeries -> new StochasticOscillatorDIndicator(new StochasticOscillatorKIndicator(timeSeries, 20)), // reference TA4J indicator
                decimalDecimalComparationFunction, // results comparation function
                tick -> tick, // function that transforms our tick to data type that is consumed by tested RxIndicator (in this case it's tick, but for EMA is for example ClosePrice)
                new RxStochasticOscillatorDIndicator(new RxStochasticOscillatorKIndicator(20)) // Rx indicator
        );

        tester.test(
                t -> new StochasticOscillatorKIndicator(t, 20),
                decimalDecimalComparationFunction,
                tick -> tick,
                new RxStochasticOscillatorKIndicator(20)
        );

        tester.test(
                timeSeries -> new ParabolicSARIndicator(timeSeries, 0.02, 0.02, 0.2),
                decimalDecimalComparationFunction,
                tick -> tick,
                new RxSARIndicator(0.02, 0.02, 0.2)
        );

        tester.test(
                t -> new HighestValueIndicator(new ClosePriceIndicator(t), 20),
                decimalDecimalComparationFunction,
                Tick::getClosePrice,
                new RxHighestValueIndicator(20)
        );

        tester.test(
                t -> new RSIIndicator(new ClosePriceIndicator(t), 20),
                decimalDecimalComparationFunction,
                Tick::getClosePrice,
                new RxRSIIndicator(20)
        );

        tester.test(
                t -> new AverageGainIndicator(new ClosePriceIndicator(t), 20),
                decimalDecimalComparationFunction,
                Tick::getClosePrice,
                new RxAverageGainIndicator(20)
        );

        tester.test(
                t -> new AverageLossIndicator(new ClosePriceIndicator(t), 20),
                decimalDecimalComparationFunction,
                Tick::getClosePrice,
                new RxAverageLossIndicator(20)
        );

        tester.test(
                t -> new CumulatedGainsIndicator(new ClosePriceIndicator(t), 20),
                decimalDecimalComparationFunction,
                Tick::getClosePrice,
                new RxCumulatedGainsIndicator(20)
        );

        tester.test(
                t -> new CumulatedLossesIndicator(new ClosePriceIndicator(t), 20),
                decimalDecimalComparationFunction,
                Tick::getClosePrice,
                new RxCumulatedLossesIndicator(20)
        );

        tester.test(
                t -> new MACDIndicator(new ClosePriceIndicator(t), 10, 20),
                decimalDecimalComparationFunction,
                Tick::getClosePrice,
                new RxMACDIndicator(10, 20));

        tester.test(
                t -> new SMAIndicator(new ClosePriceIndicator(t), 20),
                decimalDecimalComparationFunction,
                Tick::getClosePrice,
                new RxSMAIndicator(20));

        tester.test(
                t -> new EMAIndicator(new ClosePriceIndicator(t), 20),
                decimalDecimalComparationFunction,
                Tick::getClosePrice,
                new RxEMAIndicator(20));

    }
}
