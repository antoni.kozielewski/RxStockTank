package org.hou.stocktank.rx.indicators;


import io.reactivex.Flowable;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hou.stocktank.rx.providers.bossa.RxBossaSingleFileStocksDailyStreamsProvider;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;
import java.util.function.Function;

public class TA4jvsRXTester {
    private static Log LOGGER = LogFactory.getLog(TA4jvsRXTester.class);
    private static String sourceFilename;
    private static List<String> stockNames;
    private TA4JBossaSingleFileDataProviderService dataProviderService;
    private RxBossaSingleFileStocksDailyStreamsProvider rxDataProvider;

    public TA4jvsRXTester(List<String> stocks, String sourceFileName) {
        this.stockNames = stocks;
        this.sourceFilename = sourceFileName;
        dataProviderService = new TA4JBossaSingleFileDataProviderService();
        dataProviderService.setDataPath(sourceFilename);
        rxDataProvider = new RxBossaSingleFileStocksDailyStreamsProvider(sourceFilename);
    }

    private TimeSeries getTimeSeries(String stockName) {
        return dataProviderService.load(stockName);
    }

    private Flowable<org.hou.stocktank.rx.base.Tick> getTickStream(String stockName) {
        return rxDataProvider.getStream(stockName);
    }

    private <TA4JRESULT, RXINPUT, RXRESULT> String testThatShit(
            String stockName,
            Indicator<TA4JRESULT> ta4jIndicator,
            BiFunction<TA4JRESULT, RXRESULT, Boolean> compareResultsFunction,
            Function<org.hou.stocktank.rx.base.Tick, RXINPUT> rxStreamPrepareFunction,
            RxIndicator<RXINPUT, RXRESULT> rxIndicator) {

        Flowable.just(rxDataProvider.getStockStream(stockName))
                .subscribe(stockSource -> {
                    AtomicInteger index = new AtomicInteger(0);
                    stockSource
                            .getData()
                            .zipWith(
                                    rxIndicator.calculate(stockSource.getData().map(rxStreamPrepareFunction::apply)),
                                    (tick, rxValue) -> {
                                        TA4JRESULT ta4jValue = ta4jIndicator.getValue(index.getAndIncrement());
                                        if (!compareResultsFunction.apply(ta4jValue, rxValue)) {
                                            StringBuilder sb = new StringBuilder();
                                            sb.append("ERROR !!! ");
                                            sb.append(ta4jIndicator.getClass().getName() + " {" + ta4jValue + "} ");
                                            sb.append(" VS ");
                                            sb.append(rxIndicator.getClass().getName() + " {" + rxValue + "} | ");
                                            sb.append(" stock=" + stockSource.getStockName() + " | ");
                                            sb.append(" tick=" + index);
                                            throw new RuntimeException(sb.toString());
                                        }
                                        return true;
                                    })
                            .subscribe();

                    if (index.get() == 0) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("ERROR !!! ");
                        sb.append(ta4jIndicator.getClass().getName());
                        sb.append(" VS ");
                        sb.append(rxIndicator.getClass().getName());
                        sb.append(" :: RxIndicator does not produce any data !!!!!");
                        throw new RuntimeException(sb.toString());
                    }

                });
        return stockName + " : " + ta4jIndicator.getClass().getName() + " : " + rxIndicator.getClass().getName() + " ===> OK ";
    }


    public <TA4JRESULT, RXINPUT, RXRESULT> Boolean test(
            Function<TimeSeries, Indicator<TA4JRESULT>> indicatorCreationFunction,
            BiFunction<TA4JRESULT, RXRESULT, Boolean> compareResultsFunction,
            Function<org.hou.stocktank.rx.base.Tick, RXINPUT> rxStreamPrepareFunction,
            RxIndicator<RXINPUT, RXRESULT> rxIndicator
    ) {

        System.out.println();
        System.out.println(rxIndicator.getClass().getName());
        System.out.println();
        Flowable.fromIterable(stockNames)
                .parallel(4)
                .map(stockName -> testThatShit(
                        stockName,
                        indicatorCreationFunction.apply(getTimeSeries(stockName)),
                        compareResultsFunction,
                        rxStreamPrepareFunction,
                        rxIndicator
                        )
                )
                .sequential()
                .subscribe(result -> System.out.println(result));

        System.out.println(" ---------------------------------------------------------------------------------- ");
        return true;
    }

}
